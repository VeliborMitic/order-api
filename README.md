# Order Taking Rest Api

- JWT token authentication
- Role based authorization
- PostgreSQL database
- Mail sending via SMTP
- SMS sending via Twilio
- Junit tests
- Postman collection
- Docker

## Setup

### API Docs

```
http://dev.order.com/api/swagger/swagger-ui.html
```

### Postman Collection

```
docs/An-Order-taking-API.postman_collection.json
docs/Postman Environment-dev.order.com.png
```

### Credentials

```
admin@x_company.com     123456
erp@x_company.com       123456
crm@x_company.com       123456
```

### After Cloning

- Go to branch
- then run bottom docker command

### Docker

Edit `/etc/hosts`:

```
127.0.0.1 dev.order.com
127.0.0.1 pgadmin4.dev.order.com
```

Run:

```
docker-compose up --build --remove-orphans
```

Database management:
```
http://pgadmin4.dev.order.com
username: admin@x-orders.com
password: 1234
```
