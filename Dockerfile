FROM openjdk:8-jdk
WORKDIR /code
COPY . /code
RUN ./mvnw package -Dmaven.test.skip=true
RUN cp target/order-taking-api-0.0.1-SNAPSHOT.jar target/app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/code/target/app.jar"]