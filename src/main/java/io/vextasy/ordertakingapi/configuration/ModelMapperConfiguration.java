package io.vextasy.ordertakingapi.configuration;

import io.vextasy.ordertakingapi.entity.Order;
import io.vextasy.ordertakingapi.entity.Product;
import io.vextasy.ordertakingapi.entity.ProductType;
import io.vextasy.ordertakingapi.model.order.OrderGetModel;
import io.vextasy.ordertakingapi.model.product.ProductPostModel;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfiguration {

    @Bean
    public ModelMapper modelMapper() {

        ModelMapper modelMapper = new ModelMapper();

        modelMapper.createTypeMap(ProductPostModel.class, Product.class)
                .addMappings(mapper -> mapper.using(parseProductEnum())
                        .map(ProductPostModel::getProductType, Product::setProductType));

        return modelMapper;
    }

    private Converter<String, ProductType> parseProductEnum() {
        return mappingContext -> {
            if (mappingContext.getSource() != null) {
                return ProductType.getByProductTypeName(mappingContext.getSource());
            }
            return null;
        };
    }
}
