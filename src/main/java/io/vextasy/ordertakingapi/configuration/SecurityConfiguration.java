package io.vextasy.ordertakingapi.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import io.vextasy.ordertakingapi.security.AuthenticationEntryPointImpl;
import io.vextasy.ordertakingapi.security.JsonWebTokenProvider;
import io.vextasy.ordertakingapi.security.filter.JwtAuthenticationFilter;
import io.vextasy.ordertakingapi.security.filter.JwtAuthorizationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JsonWebTokenConfiguration configuration;
    private final JwtAuthorizationFilter jwtAuthorizationFilter;
    private final ObjectMapper objectMapper;
    private final JsonWebTokenProvider jsonWebTokenProvider;
    private final AuthenticationEntryPointImpl authenticationEntryPoint;

    @Value("${api.base.uri}")
    private String baseUri;

    private String getBaseUri() {
        return baseUri;
    }

    @Autowired
    public SecurityConfiguration(@Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService,
                                 BCryptPasswordEncoder bCryptPasswordEncoder,
                                 JsonWebTokenConfiguration configuration,
                                 JwtAuthorizationFilter jwtAuthorizationFilter,
                                 ObjectMapper objectMapper,
                                 JsonWebTokenProvider jsonWebTokenProvider,
                                 AuthenticationEntryPointImpl authenticationEntryPoint) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.configuration = configuration;
        this.jwtAuthorizationFilter = jwtAuthorizationFilter;
        this.objectMapper = objectMapper;
        this.jsonWebTokenProvider = jsonWebTokenProvider;
        this.authenticationEntryPoint = authenticationEntryPoint;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .addFilter(new JwtAuthenticationFilter(
                        objectMapper,
                        jsonWebTokenProvider,
                        authenticationManager(),
                        configuration
                ))
                .addFilterAfter(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,
                        "/",
                        "/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/configuration/**",
                        "/swagger-ui.html",
                        "/webjars/**")
                .permitAll()
                .antMatchers(HttpMethod.GET,
                        getBaseUri() + "/resources/**",
                        getBaseUri() + "/accounts/forgotten-password",
                        getBaseUri() + "/accounts/check-email-availability/**",
                        getBaseUri() + "/accounts/register/**")
                .permitAll()
                .antMatchers(HttpMethod.PUT,
                        getBaseUri() + "/users/password-reset")
                .permitAll()
                .antMatchers(HttpMethod.POST,
                        getBaseUri() + "/contact-us",
                        getBaseUri() + "/accounts/register/**",
                        getBaseUri() + "/accounts/forgotten-password"
                ).permitAll()
                .antMatchers(HttpMethod.POST, configuration.getEntryPoint())
                .permitAll()
                .anyRequest().authenticated();
    }
}