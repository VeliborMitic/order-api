package io.vextasy.ordertakingapi.error;

import lombok.Getter;

@Getter
public class ApiError {
    private final String field;
    private final Object rejectedValue;
    private final String message;
    private final String path;

    public ApiError(String field, Object rejectedValue, String message, String path) {
        this.field = field;
        this.rejectedValue = rejectedValue;
        this.message = message;
        this.path = path;
    }
}
