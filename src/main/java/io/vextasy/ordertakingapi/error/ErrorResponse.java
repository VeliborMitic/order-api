package io.vextasy.ordertakingapi.error;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ErrorResponse {
    private String message;
    private List<ApiError> errors;

    public ErrorResponse(String message) {
        this.message = message;
    }

    public ErrorResponse(String message, List<ApiError> errors) {
        this.message = message;
        this.errors = errors;
    }
}
