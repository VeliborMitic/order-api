package io.vextasy.ordertakingapi.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.vextasy.ordertakingapi.annotation.authorization.AuthorizeCRM;
import io.vextasy.ordertakingapi.annotation.authorization.AuthorizeERP;
import io.vextasy.ordertakingapi.annotation.swagger.GetAllResponses;
import io.vextasy.ordertakingapi.annotation.swagger.GetByIdResponses;
import io.vextasy.ordertakingapi.annotation.swagger.PostResponses;
import io.vextasy.ordertakingapi.annotation.swagger.PutResponses;
import io.vextasy.ordertakingapi.entity.Customer;
import io.vextasy.ordertakingapi.entity.Order;
import io.vextasy.ordertakingapi.entity.OrderStatus;
import io.vextasy.ordertakingapi.error.ApiError;
import io.vextasy.ordertakingapi.error.ErrorResponse;
import io.vextasy.ordertakingapi.model.order.OrderGetModel;
import io.vextasy.ordertakingapi.model.order.OrderPostModel;
import io.vextasy.ordertakingapi.model.order.OrderPutModel;
import io.vextasy.ordertakingapi.service.CustomerService;
import io.vextasy.ordertakingapi.service.OrderService;
import io.vextasy.ordertakingapi.service.ProductService;
import io.vextasy.ordertakingapi.service.email.EmailService;
import io.vextasy.ordertakingapi.service.sms.SmsSendingService;
import io.vextasy.ordertakingapi.util.Mapper;
import io.vextasy.ordertakingapi.validation.order.ValidOrderId;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static io.vextasy.ordertakingapi.util.ApiConstants.SERVICE_NOT_AVAILABLE;

@Api(value = "Order management", tags = "Order management")
@Validated
@RestController
@RequestMapping("${api.base.uri}/orders")
public class OrderController {

    private static final Logger LOGGER = LogManager.getLogger(OrderController.class);

    private final OrderService orderService;
    private final CustomerService customerService;
    private final ProductService productService;
    private final ModelMapper modelMapper;
    private final Mapper mapper;
    private final EmailService emailService;
    private final SmsSendingService smsSendingService;

    @Autowired
    public OrderController(OrderService orderService,
                           CustomerService customerService,
                           ProductService productService,
                           ModelMapper modelMapper, Mapper mapper, EmailService emailService, SmsSendingService smsSendingService) {
        this.orderService = orderService;
        this.customerService = customerService;
        this.productService = productService;
        this.modelMapper = modelMapper;
        this.mapper = mapper;
        this.emailService = emailService;
        this.smsSendingService = smsSendingService;
    }

    @ApiOperation(value = "Get list of all Orders", response = OrderGetModel.class, responseContainer = "List")
    @GetAllResponses
    @GetMapping
    public HttpEntity getAll() {

        List<OrderGetModel> orderGetModels = new ArrayList<>();
        orderService.get().forEach(order -> orderGetModels.add(mapper.mapCustomerIntoGetModel(order)));

        return ResponseEntity.ok(orderGetModels);
    }

    @ApiOperation(value = "Get single Order by Id", response = OrderGetModel.class)
    @GetByIdResponses
    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable("id") @ValidOrderId UUID id) {

        return orderService.get(id)
                .map(order -> ResponseEntity.ok(mapper.mapCustomerIntoGetModel(order)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @AuthorizeERP
    @ApiOperation(value = "Create new Order", response = OrderGetModel.class, notes = "ERP_ACCOUNT authorization required")
    @PostResponses
    @PostMapping
    public HttpEntity create(@RequestBody @Valid OrderPostModel orderPostModel, HttpServletRequest request) {

        Order createdOrder = new Order();
        modelMapper.map(orderPostModel, createdOrder);

        Customer newCustomer = new Customer();

        if (orderPostModel.getCustomerId() == null) {
            Customer createdCustomer = new Customer();
            modelMapper.map(orderPostModel, createdCustomer);
            if (customerService.get().stream()
                    .anyMatch(e -> (!Objects.equals(e.getId(), orderPostModel.getCustomerId()) && e.getEmail().equalsIgnoreCase(orderPostModel.getEmail())))) {
                List<ApiError> errors = new ArrayList<>();
                errors.add(new ApiError("email", orderPostModel.getEmail(), "Customer with same email already exists in DB", request.getRequestURI()));
                return new ResponseEntity<>(new ErrorResponse("Conflict!", errors), HttpStatus.CONFLICT);
            }

            newCustomer = customerService.create(createdCustomer).get();

        } else {
            Optional<Customer> optionalCustomer = customerService.get(orderPostModel.getCustomerId());
            if (optionalCustomer.isPresent()) {
                modelMapper.map(orderPostModel, optionalCustomer.get());
                if (customerService.get().stream()
                        .anyMatch(e -> (!Objects.equals(e.getId(), orderPostModel.getCustomerId()) && e.getEmail().equalsIgnoreCase(orderPostModel.getEmail())))) {
                    List<ApiError> errors = new ArrayList<>();
                    errors.add(new ApiError("email", orderPostModel.getEmail(), "Customer with same email already exists in DB", request.getRequestURI()));
                    return new ResponseEntity<>(new ErrorResponse("Conflict!", errors), HttpStatus.CONFLICT);
                }

                newCustomer = customerService.update(optionalCustomer.get()).get();
            }
        }

        createdOrder.setCustomer(newCustomer);
        createdOrder.setProducts(orderPostModel.getProductIds().stream()
                .map(productService::get)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList()));
        createdOrder.setOrderStatus(OrderStatus.ACCEPTED);

        return orderService.create(createdOrder).<HttpEntity>map(order -> {
            LOGGER.info("Created order {}", order.getId());
            try {
                emailService.sendMail(order);
            } catch (MessagingException e) {
                e.printStackTrace();
            }

            smsSendingService.sendSms(order);

            return ResponseEntity.created(URI.create(String.format("%s%s%s", request.getRequestURI(), "/", order.getId())))
                    .body(mapper.mapCustomerIntoGetModel(order));
        })
                .orElseGet(() -> {
                    LOGGER.error("Error while creating Order");
                    return ResponseEntity.status(HttpStatus.CONFLICT).build();
                });
    }

    @AuthorizeCRM
    @ApiOperation(value = "Update Order", response = OrderGetModel.class, notes = "ROLE_CRM_ACCOUNT authorization required")
    @PutResponses
    @PutMapping("/{id}")
    public HttpEntity update(@PathVariable("id") @ValidOrderId UUID id,
                             @RequestBody @Valid OrderPutModel orderPutModel,
                             HttpServletRequest request) {

        Optional<Order> optionalOrder = orderService.get(id);
        if (!optionalOrder.isPresent()) {
            List<ApiError> errors = new ArrayList<>();
            errors.add(new ApiError("id", id, "Order with referenced Id does not exist in DB", request.getRequestURI()));
            return new ResponseEntity<>(new ErrorResponse("Conflict!", errors), HttpStatus.CONFLICT);
        }

        Order mappedOrder = modelMapper.map(optionalOrder.get(), Order.class);
        mappedOrder.setInstallationAddress(orderPutModel.getInstallationAddress());
        mappedOrder.setPreferredInstallationDate(orderPutModel.getPreferredInstallationDate());
        mappedOrder.setFromTime(orderPutModel.getFromTime());
        mappedOrder.setToTime(orderPutModel.getToTime());

        mappedOrder.setProducts(orderPutModel.getProductIds().stream()
                .map(productService::get)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList()));

        mappedOrder.setOrderStatus(OrderStatus.valueOf(orderPutModel.getOrderStatus()));

        return orderService.update(mappedOrder).<HttpEntity>map(order -> {
            LOGGER.info("Order updated '{}'", order.getCustomer().getEmail());
            return ResponseEntity.ok(mapper.mapCustomerIntoGetModel(order));
        })
                .orElseGet(() -> {
                    LOGGER.error("Error while updating Order");
                    return new ResponseEntity<>(new ErrorResponse(SERVICE_NOT_AVAILABLE), HttpStatus.BAD_GATEWAY);
                });
    }


}
