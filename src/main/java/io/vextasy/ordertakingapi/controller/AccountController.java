package io.vextasy.ordertakingapi.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.vextasy.ordertakingapi.annotation.authorization.AuthorizeAdmin;
import io.vextasy.ordertakingapi.annotation.swagger.DeleteResponses;
import io.vextasy.ordertakingapi.annotation.swagger.GetAllResponses;
import io.vextasy.ordertakingapi.annotation.swagger.GetByIdResponses;
import io.vextasy.ordertakingapi.annotation.swagger.PostResponses;
import io.vextasy.ordertakingapi.annotation.swagger.PutResponses;
import io.vextasy.ordertakingapi.entity.Account;
import io.vextasy.ordertakingapi.error.ApiError;
import io.vextasy.ordertakingapi.error.ErrorResponse;
import io.vextasy.ordertakingapi.model.account.AccountGetModel;
import io.vextasy.ordertakingapi.model.account.AccountPostModel;
import io.vextasy.ordertakingapi.service.AccountService;
import io.vextasy.ordertakingapi.validation.account.ValidAccountId;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static io.vextasy.ordertakingapi.util.ApiConstants.SERVICE_NOT_AVAILABLE;

@Api(value = "Account management", tags = "Account management")
@Validated
@RestController
@RequestMapping("${api.base.uri}/accounts")
public class AccountController {

    private static final Logger LOGGER = LogManager.getLogger(AccountController.class);

    private final AccountService accountService;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public AccountController(AccountService accountService, ModelMapper modelMapper, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.accountService = accountService;
        this.modelMapper = modelMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @AuthorizeAdmin
    @ApiOperation(value = "Get list of all Accounts", response = AccountGetModel.class, responseContainer = "List")
    @GetAllResponses
    @GetMapping
    public HttpEntity getAll() {
        return ResponseEntity.ok(accountService.get().stream()
                .map(e -> modelMapper.map(e, AccountGetModel.class))
                .collect(Collectors.toList()));
    }

    @AuthorizeAdmin
    @ApiOperation(value = "Get single Account by Id", response = AccountGetModel.class)
    @GetByIdResponses
    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable("id") @ValidAccountId UUID id) {

        return accountService.get(id).map(e -> ResponseEntity.ok(modelMapper.map(e, AccountGetModel.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @AuthorizeAdmin
    @ApiOperation(value = "Create new Account", response = AccountGetModel.class, notes = "ADMIN authorization required")
    @PostResponses
    @PostMapping
    public HttpEntity create(@RequestBody @Valid AccountPostModel accountPostModel, HttpServletRequest request) {

        if (accountService.get().stream()
                .anyMatch(e -> e.getEmail().equalsIgnoreCase(accountPostModel.getEmail()))) {
            List<ApiError> errors = new ArrayList<>();
            errors.add(new ApiError("name", accountPostModel.getEmail(), "Account with same email already exists in DB", request.getRequestURI()));
            return new ResponseEntity<>(new ErrorResponse("Conflict!", errors), HttpStatus.CONFLICT);
        }

        Account mappedAccount = modelMapper.map(accountPostModel, Account.class);
        mappedAccount.setPassword(bCryptPasswordEncoder.encode(accountPostModel.getPassword()));

        return accountService.create(mappedAccount).<HttpEntity>map(e -> {
            LOGGER.info("Created account {}", e.getEmail());
            return ResponseEntity.created(URI.create(String.format("%s%s%s", request.getRequestURI(), "/", e.getId())))
                    .body(modelMapper.map(e, AccountGetModel.class));
        })
                .orElseGet(() -> {
                    LOGGER.error("Error while creating Account");
                    return ResponseEntity.status(HttpStatus.CONFLICT).build();
                });
    }

    @AuthorizeAdmin
    @ApiOperation(value = "Update Account", response = AccountGetModel.class, notes = "ADMIN authorization required")
    @PutResponses
    @PutMapping("/{id}")
    public HttpEntity update(@PathVariable("id") @ValidAccountId UUID id,
                             @RequestBody @Valid AccountPostModel accountPostModel,
                             HttpServletRequest request) {

        if (accountService.get().stream()
                .anyMatch(e -> (!Objects.equals(e.getId(), id) && e.getEmail().equalsIgnoreCase(accountPostModel.getEmail())))) {
            List<ApiError> errors = new ArrayList<>();
            errors.add(new ApiError("name", accountPostModel.getEmail(), "Account with same email already exists in DB", request.getRequestURI()));
            return new ResponseEntity<>(new ErrorResponse("Conflict!", errors), HttpStatus.CONFLICT);
        }

        Account mappedAccount = modelMapper.map(accountPostModel, Account.class);
        mappedAccount.setPassword(bCryptPasswordEncoder.encode(accountPostModel.getPassword()));
        mappedAccount.setId(id);

        return accountService.update(mappedAccount).<HttpEntity>map(e -> {
            LOGGER.info("Account updated '{}'", e.getEmail());
            return ResponseEntity.ok(modelMapper.map(e, AccountGetModel.class));
        })
                .orElseGet(() -> {
                    LOGGER.error("Error while updating Account");
                    return new ResponseEntity<>(new ErrorResponse(SERVICE_NOT_AVAILABLE), HttpStatus.BAD_GATEWAY);
                });
    }

    @AuthorizeAdmin
    @ApiOperation(value = "Delete Account", notes = "ADMIN authorization required")
    @DeleteResponses
    @DeleteMapping("/{id}")
    public HttpEntity delete(@PathVariable("id") @ValidAccountId UUID id, HttpServletRequest request) {
        Optional<Account> optionalCAccount = accountService.get(id);

        return optionalCAccount.<HttpEntity>map(e -> {
            accountService.delete(id);
            return ResponseEntity.noContent().build();
        })
                .orElseGet(() -> {
                    List<ApiError> errors = new ArrayList<>();
                    errors.add(new ApiError("id", id, "Account with referenced Id not found in DB", request.getRequestURI()));
                    return new ResponseEntity<>((new ErrorResponse("Conflict!", errors)), HttpStatus.NOT_FOUND);
                });
    }

    @AuthorizeAdmin
    @ApiOperation(value = "Deactivate Account", notes = "ADMIN authorization required")
    @DeleteResponses
    @PatchMapping("/{id}")
    public HttpEntity deactivateAccount(@PathVariable("id") @ValidAccountId UUID id, HttpServletRequest request) {
        Optional<Account> optionalAccount = accountService.get(id);

        return optionalAccount.<HttpEntity>map(e -> {
            e.setActive(false);
            accountService.update(e);
            return ResponseEntity.ok(modelMapper.map(e, AccountGetModel.class));
        })
                .orElseGet(() -> {
                    List<ApiError> errors = new ArrayList<>();
                    errors.add(new ApiError("id", id, "Account with referenced Id not found in DB", request.getRequestURI()));
                    return new ResponseEntity<>((new ErrorResponse("Conflict!", errors)), HttpStatus.NOT_FOUND);
                });
    }
}
