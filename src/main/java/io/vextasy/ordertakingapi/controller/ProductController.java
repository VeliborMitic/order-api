package io.vextasy.ordertakingapi.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.vextasy.ordertakingapi.annotation.authorization.AuthorizeAdmin;
import io.vextasy.ordertakingapi.annotation.swagger.DeleteResponses;
import io.vextasy.ordertakingapi.annotation.swagger.GetAllResponses;
import io.vextasy.ordertakingapi.annotation.swagger.GetByIdResponses;
import io.vextasy.ordertakingapi.annotation.swagger.PostResponses;
import io.vextasy.ordertakingapi.annotation.swagger.PutResponses;
import io.vextasy.ordertakingapi.entity.Product;
import io.vextasy.ordertakingapi.error.ApiError;
import io.vextasy.ordertakingapi.error.ErrorResponse;
import io.vextasy.ordertakingapi.model.product.ProductGetModel;
import io.vextasy.ordertakingapi.model.product.ProductPostModel;
import io.vextasy.ordertakingapi.service.ProductService;
import io.vextasy.ordertakingapi.validation.product.ValidProductId;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


import static io.vextasy.ordertakingapi.util.ApiConstants.SERVICE_NOT_AVAILABLE;

@Api(value = "Product management", tags = "Product management")
@Validated
@RestController
@RequestMapping("${api.base.uri}/products")
public class ProductController {

    private static final Logger LOGGER = LogManager.getLogger(ProductController.class);

    private final ProductService productService;
    private final ModelMapper modelMapper;

    @Autowired
    public ProductController(ProductService productService, ModelMapper modelMapper) {
        this.productService = productService;
        this.modelMapper = modelMapper;
    }

    @ApiOperation(value = "Get list of all Products", response = ProductGetModel.class, responseContainer = "List")
    @GetAllResponses
    @GetMapping
    public HttpEntity getAll() {
        return ResponseEntity.ok(productService.get().stream()
                .map(e -> modelMapper.map(e, ProductGetModel.class))
                .collect(Collectors.toList()));
    }

    @ApiOperation(value = "Get single Product by Id", response = ProductGetModel.class)
    @GetByIdResponses
    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable("id") @ValidProductId UUID id) {

        return productService.get(id).map(e -> ResponseEntity.ok(modelMapper.map(e, ProductGetModel.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @AuthorizeAdmin
    @ApiOperation(value = "Create new Product", response = ProductGetModel.class, notes = "ADMIN authorization required")
    @PostResponses
    @PostMapping
    public HttpEntity create(@RequestBody @Valid ProductPostModel productPostModel, HttpServletRequest request) {

        if (productService.get().stream()
                .anyMatch(e -> e.getName().equalsIgnoreCase(productPostModel.getName()))) {
            List<ApiError> errors = new ArrayList<>();
            errors.add(new ApiError("name", productPostModel.getName(), "Product with same name already exists in DB", request.getRequestURI()));
            return new ResponseEntity<>(new ErrorResponse("Conflict!", errors), HttpStatus.CONFLICT);
        }

        Product mappedProduct = modelMapper.map(productPostModel, Product.class);


        return productService.create(mappedProduct).<HttpEntity>map(e -> {
            LOGGER.info("Created product {}", e.getName());
            return ResponseEntity.created(URI.create(String.format("%s%s%s", request.getRequestURI(), "/", e.getId())))
                    .body(modelMapper.map(e, ProductGetModel.class));
        })
                .orElseGet(() -> {
                    LOGGER.error("Error while creating Product");
                    return ResponseEntity.status(HttpStatus.CONFLICT).build();
                });
    }

    @AuthorizeAdmin
    @ApiOperation(value = "Update Product", response = ProductGetModel.class, notes = "ADMIN authorization required")
    @PutResponses
    @PutMapping("/{id}")
    public HttpEntity update(@PathVariable("id") @ValidProductId UUID id,
                             @RequestBody @Valid ProductPostModel productPostModel,
                             HttpServletRequest request) {

        if (productService.get().stream()
                .anyMatch(e -> (!Objects.equals(e.getId(), id) && e.getName().equalsIgnoreCase(productPostModel.getName())))) {
            List<ApiError> errors = new ArrayList<>();
            errors.add(new ApiError("name", productPostModel.getName(), "Product with same name already exists in DB", request.getRequestURI()));
            return new ResponseEntity<>(new ErrorResponse("Conflict!", errors), HttpStatus.CONFLICT);
        }

        Product mappedProduct = modelMapper.map(productPostModel, Product.class);
        mappedProduct.setId(id);

        return productService.update(mappedProduct).<HttpEntity>map(e -> {
            LOGGER.info("Product updated '{}'", e.getName());
            return ResponseEntity.ok(modelMapper.map(e, ProductGetModel.class));
        })
                .orElseGet(() -> {
                    LOGGER.error("Error while updating Product");
                    return new ResponseEntity<>(new ErrorResponse(SERVICE_NOT_AVAILABLE), HttpStatus.BAD_GATEWAY);
                });

    }

    @AuthorizeAdmin
    @ApiOperation(value = "Delete Product", notes = "ADMIN authorization required")
    @DeleteResponses
    @DeleteMapping("/{id}")
    public HttpEntity delete(@PathVariable("id") @ValidProductId UUID id, HttpServletRequest request) {
        Optional<Product> optionalProduct = productService.get(id);

        return optionalProduct.<HttpEntity>map(e -> {
            productService.delete(id);
            return ResponseEntity.noContent().build();
        })
                .orElseGet(() -> {
                    List<ApiError> errors = new ArrayList<>();
                    errors.add(new ApiError("name", id, "Product with referenced Id not found in DB", request.getRequestURI()));
                    return new ResponseEntity<>((new ErrorResponse("Conflict!", errors)), HttpStatus.NOT_FOUND);
                });
    }

}
