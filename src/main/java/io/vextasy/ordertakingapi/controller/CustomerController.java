package io.vextasy.ordertakingapi.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.vextasy.ordertakingapi.annotation.authorization.AuthorizeAdmin;
import io.vextasy.ordertakingapi.annotation.swagger.GetAllResponses;
import io.vextasy.ordertakingapi.annotation.swagger.GetByIdResponses;
import io.vextasy.ordertakingapi.annotation.swagger.PostResponses;
import io.vextasy.ordertakingapi.annotation.swagger.PutResponses;
import io.vextasy.ordertakingapi.entity.Customer;
import io.vextasy.ordertakingapi.error.ApiError;
import io.vextasy.ordertakingapi.error.ErrorResponse;
import io.vextasy.ordertakingapi.model.customer.CustomerGetModel;
import io.vextasy.ordertakingapi.model.customer.CustomerPostModel;
import io.vextasy.ordertakingapi.service.CustomerService;
import io.vextasy.ordertakingapi.validation.customer.ValidCustomerId;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import static io.vextasy.ordertakingapi.util.ApiConstants.SERVICE_NOT_AVAILABLE;

@Api(value = "Customer management", tags = "Customer management")
@Validated
@RestController
@RequestMapping("${api.base.uri}/customers")
public class CustomerController {

    private static final Logger LOGGER = LogManager.getLogger(CustomerController.class);

    private final CustomerService customerService;
    private final ModelMapper modelMapper;

    @Autowired
    public CustomerController(CustomerService customerService, ModelMapper modelMapper) {
        this.customerService = customerService;
        this.modelMapper = modelMapper;
    }

    @ApiOperation(value = "Get list of all Customers", response = CustomerGetModel.class, responseContainer = "List")
    @GetAllResponses
    @GetMapping
    public HttpEntity getAll() {
        return ResponseEntity.ok(customerService.get().stream()
                .map(e -> modelMapper.map(e, CustomerGetModel.class))
                .collect(Collectors.toList()));
    }

    @ApiOperation(value = "Get single Customer by Id", response = CustomerGetModel.class)
    @GetByIdResponses
    @GetMapping("/{id}")
    public HttpEntity get(@PathVariable("id") @ValidCustomerId UUID id) {

        return customerService.get(id).map(e -> ResponseEntity.ok(modelMapper.map(e, CustomerGetModel.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @AuthorizeAdmin
    @ApiOperation(value = "Create new Customer", response = CustomerGetModel.class, notes = "ADMIN authorization required")
    @PostResponses
    @PostMapping
    public HttpEntity create(@RequestBody @Valid CustomerPostModel customerPostModel, HttpServletRequest request) {

        if (customerService.get().stream()
                .anyMatch(e -> e.getEmail().equalsIgnoreCase(customerPostModel.getEmail()))) {
            List<ApiError> errors = new ArrayList<>();
            errors.add(new ApiError("name", customerPostModel.getEmail(), "Customer with same email already exists in DB", request.getRequestURI()));
            return new ResponseEntity<>(new ErrorResponse("Conflict!", errors), HttpStatus.CONFLICT);
        }

        Customer mappedCustomer = modelMapper.map(customerPostModel, Customer.class);

        return customerService.create(mappedCustomer).<HttpEntity>map(e -> {
            LOGGER.info("Created customer {}", e.getEmail());
            return ResponseEntity.created(URI.create(String.format("%s%s%s", request.getRequestURI(), "/", e.getId())))
                    .body(modelMapper.map(e, CustomerGetModel.class));
        })
                .orElseGet(() -> {
                    LOGGER.error("Error while creating Customer");
                    return ResponseEntity.status(HttpStatus.CONFLICT).build();
                });
    }

    @AuthorizeAdmin
    @ApiOperation(value = "Update Customer", response = CustomerGetModel.class, notes = "ADMIN authorization required")
    @PutResponses
    @PutMapping("/{id}")
    public HttpEntity update(@PathVariable("id") @ValidCustomerId UUID id,
                             @RequestBody @Valid CustomerPostModel customerPostModel,
                             HttpServletRequest request) {

        if (customerService.get().stream()
                .anyMatch(e -> (!Objects.equals(e.getId(), id) && e.getEmail().equalsIgnoreCase(customerPostModel.getEmail())))) {
            List<ApiError> errors = new ArrayList<>();
            errors.add(new ApiError("name", customerPostModel.getEmail(), "Customer with same email already exists in DB", request.getRequestURI()));
            return new ResponseEntity<>(new ErrorResponse("Conflict!", errors), HttpStatus.CONFLICT);
        }

        Customer mappedCustomer = modelMapper.map(customerPostModel, Customer.class);
        mappedCustomer.setId(id);

        return customerService.update(mappedCustomer).<HttpEntity>map(e -> {
            LOGGER.info("Customer updated '{}'", e.getEmail());
            return ResponseEntity.ok(modelMapper.map(e, CustomerGetModel.class));
        })
                .orElseGet(() -> {
                    LOGGER.error("Error while updating Customer");
                    return new ResponseEntity<>(new ErrorResponse(SERVICE_NOT_AVAILABLE), HttpStatus.BAD_GATEWAY);
                });
    }
}
