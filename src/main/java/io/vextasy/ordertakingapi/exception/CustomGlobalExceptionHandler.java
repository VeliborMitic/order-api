package io.vextasy.ordertakingapi.exception;

import io.vextasy.ordertakingapi.error.ApiError;
import io.vextasy.ordertakingapi.error.ErrorResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import io.vextasy.ordertakingapi.model.AuthResponse;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static io.vextasy.ordertakingapi.util.ApiConstants.INVALID_OBJECT;
import static io.vextasy.ordertakingapi.util.ApiConstants.VALIDATION_FAILED;

@ControllerAdvice
public class CustomGlobalExceptionHandler {

    private static final Logger LOGGER = LogManager.getLogger(CustomGlobalExceptionHandler.class);

    @ExceptionHandler(ConstraintViolationException.class)
    public HttpEntity handleConstraintViolationException(ConstraintViolationException ex, HttpServletRequest request) {
        List<ApiError> errorList = ex.getConstraintViolations().stream()
                .map(cv -> new ApiError(cv.getPropertyPath().toString(), cv.getInvalidValue(), cv.getMessage(), request.getRequestURI()))
                .collect(Collectors.toList());
        LOGGER.error(ex.getMessage());

        return new ResponseEntity<>(new ErrorResponse(VALIDATION_FAILED, errorList), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({AccessDeniedException.class})
    public HttpEntity handleAccessDeniedException(AccessDeniedException ex) {

        LOGGER.error(ex.getMessage());
        return new ResponseEntity<>(new AuthResponse("Authorization fail", ex.getMessage()), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, MethodArgumentTypeMismatchException.class})
    public HttpEntity handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, HttpServletRequest request) {

        List<ApiError> errorList = ex.getBindingResult().getFieldErrors().stream()
                .map(fieldError -> new ApiError(fieldError.getField(), fieldError.getRejectedValue(), fieldError.getDefaultMessage(), request.getRequestURI()))
                .collect(Collectors.toList());

        LOGGER.error(ex.getMessage());
        return new ResponseEntity<>(new ErrorResponse(VALIDATION_FAILED, errorList), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public HttpEntity handleIllegalArgumentException(IllegalArgumentException ex) {

        return new ResponseEntity<>(new ErrorResponse(ex.getMessage(), null), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public HttpEntity handleDataIntegrityViolationException(DataIntegrityViolationException ex, HttpServletRequest request) {

        List<ApiError> errorList = new ArrayList<>();
        errorList.add(new ApiError(null, ex.getCause().getCause().getMessage(), INVALID_OBJECT, request.getRequestURI()));

        LOGGER.error(ex.getMessage());
        return new ResponseEntity<>(new ErrorResponse(ex.getCause().getMessage() + " - " + INVALID_OBJECT, errorList), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({RuntimeException.class})
    public HttpEntity handleRuntimeException(RuntimeException ex, HttpServletRequest request) {

        List<ApiError> errorList = new ArrayList<>();
        errorList.add(new ApiError(null, ex.getCause().getCause().getMessage(), INVALID_OBJECT, request.getRequestURI()));

        LOGGER.error(ex.getMessage());
        return new ResponseEntity<>(new ErrorResponse(ex.getCause().getMessage() + " - " + INVALID_OBJECT, errorList), HttpStatus.BAD_REQUEST);
    }

}
