package io.vextasy.ordertakingapi.util;

import io.vextasy.ordertakingapi.entity.Order;
import io.vextasy.ordertakingapi.entity.Product;
import io.vextasy.ordertakingapi.model.order.OrderGetModel;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class Mapper {

    private final ModelMapper modelMapper;

    public Mapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public OrderGetModel mapCustomerIntoGetModel(Order order){
        OrderGetModel model = modelMapper.map(order, OrderGetModel.class);
        model.setFirstName(order.getCustomer().getFirstName());
        model.setLastName(order.getCustomer().getLastName());
        model.setEmail(order.getCustomer().getEmail());
        model.setPhoneNumber(order.getCustomer().getPhoneNumber());
        model.setBillingAddress(order.getCustomer().getBillingAddress());
        model.setProductIds(order.getProducts().stream().map(Product::getId).collect(Collectors.toList()));

        return model;
    }
}
