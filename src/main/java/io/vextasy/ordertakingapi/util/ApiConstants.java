package io.vextasy.ordertakingapi.util;

public class ApiConstants {

    public static final String SERVICE_NOT_AVAILABLE = "The service is not available right now, try again later";
    public static final String VALIDATION_FAILED = "Request validation failed";
    public static final String INVALID_OBJECT = "Invalid object received";

    private ApiConstants() {
    }
}
