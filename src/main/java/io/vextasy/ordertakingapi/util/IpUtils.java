package io.vextasy.ordertakingapi.util;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

public class IpUtils {

    private IpUtils() {
    }

    private static final String[] IP_HEADER_NAMES = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"
    };

    public static String fetchClientIp(RequestAttributes requestAttributes) {
        if (requestAttributes == null) {
            return "0.0.0.0";
        }

        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        String ip = Arrays.stream(IP_HEADER_NAMES)
                .map(request::getHeader)
                .filter(IpUtils::test)
                .map(header -> header.split(",")[0])
                .reduce("", (h1, h2) -> h1 + ":" + h2);

        if (ip.equals("")) {
            ip = ip + request.getRemoteAddr();
        }

        if (ip.equals("0:0:0:0:0:0:0:1")) {
            return "127.0.0.1";
        }

        return ip;
    }

    private static boolean test(String header) {
        return header != null && header.length() != 0 && !"unknown".equalsIgnoreCase(header);
    }
}