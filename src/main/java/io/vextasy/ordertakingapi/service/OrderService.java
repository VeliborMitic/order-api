package io.vextasy.ordertakingapi.service;

import io.vextasy.ordertakingapi.entity.Order;

public interface OrderService extends BaseService<Order> {
}
