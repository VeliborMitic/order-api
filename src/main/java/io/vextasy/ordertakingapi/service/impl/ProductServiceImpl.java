package io.vextasy.ordertakingapi.service.impl;

import io.vextasy.ordertakingapi.entity.Product;
import io.vextasy.ordertakingapi.repository.ProductRepository;
import io.vextasy.ordertakingapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        super(productRepository);
        this.productRepository = productRepository;
    }

}
