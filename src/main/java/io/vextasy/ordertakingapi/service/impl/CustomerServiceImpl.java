package io.vextasy.ordertakingapi.service.impl;

import io.vextasy.ordertakingapi.entity.Customer;
import io.vextasy.ordertakingapi.repository.CustomerRepository;
import io.vextasy.ordertakingapi.service.CustomerService;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl extends BaseServiceImpl<Customer> implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        super(customerRepository);
        this.customerRepository = customerRepository;
    }
}
