package io.vextasy.ordertakingapi.service.impl;

import io.vextasy.ordertakingapi.entity.Account;
import io.vextasy.ordertakingapi.repository.AccountRepository;
import io.vextasy.ordertakingapi.service.AccountService;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl extends BaseServiceImpl<Account> implements AccountService {

    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        super(accountRepository);
        this.accountRepository = accountRepository;
    }
}
