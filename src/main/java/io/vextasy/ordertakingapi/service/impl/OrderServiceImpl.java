package io.vextasy.ordertakingapi.service.impl;

import io.vextasy.ordertakingapi.entity.Order;
import io.vextasy.ordertakingapi.repository.OrderRepository;
import io.vextasy.ordertakingapi.service.OrderService;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl extends BaseServiceImpl<Order> implements OrderService {

    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        super(orderRepository);
        this.orderRepository = orderRepository;
    }
}
