package io.vextasy.ordertakingapi.service.impl;

import io.vextasy.ordertakingapi.entity.BaseEntity;
import io.vextasy.ordertakingapi.repository.BaseRepository;
import io.vextasy.ordertakingapi.service.BaseService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public abstract class BaseServiceImpl<T extends BaseEntity> implements BaseService<T> {

    private final BaseRepository<T> baseRepository;

    public BaseServiceImpl(BaseRepository<T> baseRepository) {
        this.baseRepository = baseRepository;
    }

    @Override
    public List<T> get() {
        return baseRepository.findAll();
    }

    @Override
    public Optional<T> get(UUID id) {
        return baseRepository.findById(id);
    }

    @Override
    public Optional<T> create(T entity) {
        return Optional.of(baseRepository.save(entity));
    }

    @Override
    public Optional<T> update(T entity) {
        if (get(entity.getId()).isPresent()) {
            return Optional.of(baseRepository.save(entity));
        }

        return Optional.empty();
    }

    @Override
    public void delete(UUID id) {
        baseRepository.deleteById(id);
    }

    @Override
    public void delete(T t) {
        baseRepository.delete(t);
    }
}