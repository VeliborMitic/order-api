package io.vextasy.ordertakingapi.service.sms;


import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import io.vextasy.ordertakingapi.entity.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SmsSendingService {

    private static final Logger LOGGER = LogManager.getLogger(SmsSendingService.class);

    @Value("${twilio.SID}")
    private String twilioSid;

    @Value("${twilio.token}")
    private String twilioToken;

    @Value("${twilio.phone-number}")
    private String twilioPhoneNumber;

    public void sendSms(Order order){
        Twilio.init(twilioSid, twilioToken);
        Message message = Message.creator(
//                new PhoneNumber(order.getCustomer().getPhoneNumber()),
                new PhoneNumber("+381652365600"),
                new PhoneNumber(twilioPhoneNumber),
                "Order confirmation! Your order to 'X-company' has been received. Orders status is " + order.getOrderStatus())
                .create();

        LOGGER.info("SMS message sent");
    }

}
