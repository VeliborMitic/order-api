package io.vextasy.ordertakingapi.service;

import io.vextasy.ordertakingapi.entity.Product;

public interface ProductService extends BaseService<Product> {
}
