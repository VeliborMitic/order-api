package io.vextasy.ordertakingapi.service;

import io.vextasy.ordertakingapi.entity.Customer;

public interface CustomerService extends BaseService<Customer> {
}
