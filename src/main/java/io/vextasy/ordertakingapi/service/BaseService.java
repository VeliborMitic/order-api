package io.vextasy.ordertakingapi.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BaseService<T> {
    List<T> get();

    Optional<T> get(UUID id);

    Optional<T> create(T entity);

    Optional<T> update(T entity);

    void delete(UUID id);

    void delete(T t);
}
