package io.vextasy.ordertakingapi.service;

import io.vextasy.ordertakingapi.entity.Account;

public interface AccountService extends BaseService<Account> {
}
