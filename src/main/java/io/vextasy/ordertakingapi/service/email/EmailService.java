package io.vextasy.ordertakingapi.service.email;

import io.vextasy.ordertakingapi.controller.OrderController;
import io.vextasy.ordertakingapi.entity.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailService {

    private static final Logger LOGGER = LogManager.getLogger(EmailService.class);

    @Autowired
    private JavaMailSender mailSender;

    public void sendMail(Order order) throws MessagingException {

        MimeMessage msg = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);

        helper.setFrom("support@x-company.com");
        helper.setTo(order.getCustomer().getEmail());
        helper.setSubject("Order confirmation");
        helper.setText("<p>Hello " + order.getCustomer().getFirstName() + " " + order.getCustomer().getLastName() + "!</p>" +
                        "<p>Order details:<br>" +
                        "Order status: " + order.getOrderStatus() + "<br>" +
                        "Instalation address: " + order.getInstallationAddress() + "<br>" +
                        "Preferred installation date: " + order.getPreferredInstallationDate() + "<br>" +
                        "Time slot: " + order.getFromTime() + " - " + order.getToTime() +
                        "</p>",
                true);

        LOGGER.info("Email sent to: {}", order.getCustomer().getEmail());
    }
}
