package io.vextasy.ordertakingapi.validation.customer;

import io.vextasy.ordertakingapi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

public class CustomerIdValidator implements ConstraintValidator<ValidCustomerId, UUID> {
    private final CustomerService customerService;

    @Autowired
    public CustomerIdValidator(CustomerService customerService) {
        this.customerService = customerService;
    }

    public boolean isValid(UUID id, ConstraintValidatorContext context){
        return customerService.get(id).isPresent();
    }
}
