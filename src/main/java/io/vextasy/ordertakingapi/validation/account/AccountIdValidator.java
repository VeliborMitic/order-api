package io.vextasy.ordertakingapi.validation.account;

import io.vextasy.ordertakingapi.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

public class AccountIdValidator implements ConstraintValidator<ValidAccountId, UUID> {
    private final AccountService accountService;

    @Autowired
    public AccountIdValidator(AccountService accountService) {
        this.accountService = accountService;
    }

    public boolean isValid(UUID id, ConstraintValidatorContext context){
        return accountService.get(id).isPresent();
    }
}
