package io.vextasy.ordertakingapi.validation.order;

import io.vextasy.ordertakingapi.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

public class OrderIdValidator implements ConstraintValidator<ValidOrderId, UUID> {
    private final OrderService orderService;

    @Autowired
    public OrderIdValidator(OrderService orderService) {
        this.orderService = orderService;
    }

    public boolean isValid(UUID id, ConstraintValidatorContext context){
        return orderService.get(id).isPresent();
    }
}
