package io.vextasy.ordertakingapi.validation.product;

import io.vextasy.ordertakingapi.entity.ProductType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class ValueOfEnumProductTypeValidator implements ConstraintValidator<ValueOfEnumProductType, String> {

    private List<String> acceptedValues;

    @Override
    public void initialize(ValueOfEnumProductType annotation) {
        acceptedValues = ProductType.getProductTypeNames();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null) {
            return true;
        }

        return acceptedValues.contains(value);
    }
}
