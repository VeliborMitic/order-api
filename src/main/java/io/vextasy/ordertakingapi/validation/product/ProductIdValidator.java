package io.vextasy.ordertakingapi.validation.product;

import io.vextasy.ordertakingapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

public class ProductIdValidator implements ConstraintValidator<ValidProductId, UUID> {
    private final ProductService productService;

    @Autowired
    public ProductIdValidator(ProductService productService) {
        this.productService = productService;
    }

    public boolean isValid(UUID id, ConstraintValidatorContext context){
        return productService.get(id).isPresent();
    }
}
