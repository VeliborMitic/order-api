package io.vextasy.ordertakingapi.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import io.vextasy.ordertakingapi.model.AuthResponse;
import io.vextasy.ordertakingapi.util.IpUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {

    private static final Logger LOGGER = LogManager.getLogger(AuthenticationEntryPointImpl.class);

    private final ObjectMapper objectMapper;

    @Autowired
    public AuthenticationEntryPointImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }


    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException {
        PrintWriter writer = response.getWriter();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        AuthResponse authResponse = new AuthResponse("ERROR", "Invalid token");
        writer.print(objectMapper.writeValueAsString(authResponse));
        writer.flush();
        LOGGER.info("Unsuccessful authentication attempt - {}, from ip address {}", authResponse.getMessage(), IpUtils.fetchClientIp(RequestContextHolder.currentRequestAttributes()));
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
}

