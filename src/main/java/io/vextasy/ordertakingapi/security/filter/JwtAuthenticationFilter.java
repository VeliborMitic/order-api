package io.vextasy.ordertakingapi.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.context.request.RequestContextHolder;
import io.vextasy.ordertakingapi.configuration.JsonWebTokenConfiguration;
import io.vextasy.ordertakingapi.model.AuthResponse;
import io.vextasy.ordertakingapi.security.CredentialsNotFoundException;
import io.vextasy.ordertakingapi.security.JsonWebTokenProvider;
import io.vextasy.ordertakingapi.security.UserCredentials;
import io.vextasy.ordertakingapi.util.IpUtils;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private static final Logger LOGGER = LogManager.getLogger(JwtAuthenticationFilter.class);

    private final ObjectMapper objectMapper;
    private final JsonWebTokenProvider jsonWebTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final JsonWebTokenConfiguration configuration;

    public JwtAuthenticationFilter(ObjectMapper objectMapper,
                                   JsonWebTokenProvider jsonWebTokenProvider,
                                   AuthenticationManager authenticationManager,
                                   JsonWebTokenConfiguration configuration) {
        this.objectMapper = objectMapper;
        this.jsonWebTokenProvider = jsonWebTokenProvider;
        this.authenticationManager = authenticationManager;
        this.configuration = configuration;

        setAuthenticationEntryPoint();
    }

    private void setAuthenticationEntryPoint() {
        String authEntryPoint = configuration.getEntryPoint();
        if (authEntryPoint != null)
            this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(
                    authEntryPoint,
                    "POST"
            ));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        try {
            UserCredentials userCredentials = objectMapper.readValue(request.getInputStream(), UserCredentials.class);
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    userCredentials.getEmail(),
                    userCredentials.getPassword());

            return authenticationManager.authenticate(authToken);
        } catch (IOException e) {
            throw new CredentialsNotFoundException("Credentials are not present.");
        } catch (AuthenticationException e) {
            throw new BadCredentialsException(e.getMessage(), e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException {
        String jwt = String.format("%s %s",
                configuration.getType(),
                jsonWebTokenProvider.generateToken(authResult));
        response.addHeader("Access-Control-Expose-Headers", "Authorization");
        response.addHeader(configuration.getHeader(), jwt);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();

        AuthResponse authResponse = new AuthResponse("SUCCESS", "SUCCESSFULLY_SIGNED_IN");
        writer.print(objectMapper.writeValueAsString(authResponse));
        writer.flush();

        LOGGER.info("Successful authentication, {},  from ip address {}",
                authResponse.getMessage(), IpUtils.fetchClientIp(RequestContextHolder.currentRequestAttributes()));
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException {
        response.setStatus(401);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();

        AuthResponse authResponse = new AuthResponse("ERROR", failed.getMessage().toUpperCase().replace(" ", "_"));
        writer.print(objectMapper.writeValueAsString(authResponse));
        writer.flush();

        LOGGER.info("Unsuccessful authentication attempt - {}, from ip address {}",
                authResponse.getMessage(), IpUtils.fetchClientIp(RequestContextHolder.currentRequestAttributes()));
    }
}
