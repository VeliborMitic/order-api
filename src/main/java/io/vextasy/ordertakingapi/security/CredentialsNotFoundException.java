package io.vextasy.ordertakingapi.security;

import org.springframework.security.core.AuthenticationException;

public class CredentialsNotFoundException extends AuthenticationException {

    public CredentialsNotFoundException(String msg) {
        super(msg);
    }
}
