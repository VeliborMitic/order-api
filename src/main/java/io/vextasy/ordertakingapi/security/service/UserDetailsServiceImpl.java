package io.vextasy.ordertakingapi.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import io.vextasy.ordertakingapi.entity.Account;
import io.vextasy.ordertakingapi.repository.AccountRepository;
import io.vextasy.ordertakingapi.security.InternalUser;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AccountRepository accountRepository;

    @Autowired
    public UserDetailsServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(final String name) {
        final Optional<Account> optionalUser = accountRepository.findByEmail(name);

        return optionalUser.map(InternalUser::create)
                .orElseThrow(() -> new UsernameNotFoundException(name));
    }
}
