package io.vextasy.ordertakingapi.security.service;

import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import io.vextasy.ordertakingapi.model.LoggedUserGetModel;
import io.vextasy.ordertakingapi.repository.AccountRepository;
import io.vextasy.ordertakingapi.security.InternalUser;

import java.util.UUID;

@Service
public class AuthService {

    private final ModelMapper modelMapper;
    private final AccountRepository accountRepository;

    public AuthService(ModelMapper modelMapper,
                       AccountRepository accountRepository) {
        this.modelMapper = modelMapper;
        this.accountRepository = accountRepository;
    }

    public InternalUser getPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        Object principal = authentication.getPrincipal();
        if (!(principal instanceof InternalUser)) {
            return null;
        }
        return (InternalUser) principal;
    }


    public boolean checkPrincipalHasId(UUID id) {
        return principalHasId(id);
    }

    private boolean principalIsAdmin() {
        InternalUser principal = getPrincipal();
        if (principal == null) {
            return false;
        }
        return principal.getAuthorities().toString().toUpperCase()
                .contains("ADMIN");
    }

    private boolean principalHasId(UUID id) {
        InternalUser principal = getPrincipal();
        if (principal == null) {
            return false;
        }
        return principal.getId().equals(id);
    }
}
