package io.vextasy.ordertakingapi.repository;

import io.vextasy.ordertakingapi.entity.Customer;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends BaseRepository<Customer>{
}
