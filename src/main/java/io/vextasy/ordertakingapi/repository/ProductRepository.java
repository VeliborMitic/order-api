package io.vextasy.ordertakingapi.repository;

import io.vextasy.ordertakingapi.entity.Product;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends BaseRepository<Product>{
}
