package io.vextasy.ordertakingapi.repository;

import io.vextasy.ordertakingapi.entity.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends BaseRepository<Order> {
}
