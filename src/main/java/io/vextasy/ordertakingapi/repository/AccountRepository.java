package io.vextasy.ordertakingapi.repository;

import org.springframework.stereotype.Repository;
import io.vextasy.ordertakingapi.entity.Account;

import java.util.Optional;

@Repository
public interface AccountRepository extends BaseRepository<Account> {

    Optional<Account> findByEmail(String email);
}
