package io.vextasy.ordertakingapi.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ProductType {
    INTERNET("Internet"),
    TV("Television"),
    TELEPHONY("Telephony"),
    MOBILE("Mobile phone");

    private static final Map<String, ProductType> PRODUCT_TYPES_BY_NAME = new HashMap<>();
    private static final Map<ProductType, String> PRODUCT_TYPE_NAMES_BY_TYPE = new HashMap<>();
    private static final List<String> PRODUCT_TYPES_NAMES_LIST = new ArrayList<>();

    static {
        Arrays.stream(values()).forEach(type -> {

            PRODUCT_TYPES_BY_NAME.put(type.productTypeName, type);
            PRODUCT_TYPE_NAMES_BY_TYPE.put(type, type.productTypeName);
            PRODUCT_TYPES_NAMES_LIST.add(type.productTypeName);

        });
    }

    private final String productTypeName;

    ProductType(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public static ProductType getByProductTypeName(String productTypeName) {
        return PRODUCT_TYPES_BY_NAME.get(productTypeName);
    }

    public static String getByProductTypeEnumValue(ProductType productType) {
        return PRODUCT_TYPE_NAMES_BY_TYPE.get(productType);
    }

    public static List<String> getProductTypeNames(){
        return PRODUCT_TYPES_NAMES_LIST;
    }

}
