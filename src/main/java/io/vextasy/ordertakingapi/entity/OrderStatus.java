package io.vextasy.ordertakingapi.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum OrderStatus {

    ACCEPTED, DONE, REJECTED;

    private static final List<OrderStatus> ORDER_STATUSES = new ArrayList<>();

    static {
        ORDER_STATUSES.addAll(Arrays.asList(values()));
    }

    public static List<OrderStatus> getAllProductTypes() {
        return ORDER_STATUSES;
    }
}
