package io.vextasy.ordertakingapi.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Table(name = "products")
public class Product extends BaseEntity{

    @Enumerated(EnumType.STRING)
    private ProductType productType;

    private BigDecimal price;

    @Column(unique = true)
    private String name;

    private String description;
}
