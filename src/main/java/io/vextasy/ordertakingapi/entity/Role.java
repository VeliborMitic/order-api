package io.vextasy.ordertakingapi.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Role {
    ROLE_ADMIN,
    ROLE_ERP_ACCOUNT,
    ROLE_CRM_ACCOUNT;

    private static final List<Role> ROLE_LIST = new ArrayList<>();

    static {
        ROLE_LIST.addAll(Arrays.asList(values()));
    }

    public static List<Role> getAllRoles() {
        return ROLE_LIST;
    }
}
