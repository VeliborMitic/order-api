package io.vextasy.ordertakingapi.model;

import lombok.Getter;
import lombok.Setter;
import io.vextasy.ordertakingapi.entity.Role;

import java.util.UUID;

@Getter
@Setter
public class LoggedUserGetModel {

    private UUID id;
    private String email;
    private Role role;
}
