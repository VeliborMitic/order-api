package io.vextasy.ordertakingapi.model.account;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AccountPostModel extends AccountBaseModel{

    @NotNull(message = "Password is required!")
    @Size(min = 6, max = 60, message = "Password must be between 6 and 60 characters long!")
    private String password;
}
