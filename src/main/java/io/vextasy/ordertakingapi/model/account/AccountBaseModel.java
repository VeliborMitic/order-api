package io.vextasy.ordertakingapi.model.account;

import io.vextasy.ordertakingapi.entity.Role;
import io.vextasy.ordertakingapi.validation.ValueOfEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AccountBaseModel {

    @NotNull
    @Email(message = "Invalid email address format!")
    private String email;

    @NotNull(message = "Active field is required")
    private boolean active;

    @NotNull(message = "Role field is required")
    @ValueOfEnum(enumClass = Role.class)
    @Enumerated(EnumType.STRING)
    private String role;
}
