package io.vextasy.ordertakingapi.model.account;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
public class AccountGetModel extends AccountBaseModel {

    private UUID id;

    private boolean verifiedEmail;

    private Instant createdAt;

    private Instant updatedAt;

    private String createdBy;

    private String lastModifiedBy;
}
