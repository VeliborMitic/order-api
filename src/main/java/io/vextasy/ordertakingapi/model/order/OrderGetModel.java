package io.vextasy.ordertakingapi.model.order;

import io.vextasy.ordertakingapi.entity.OrderStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
public class OrderGetModel extends OrderPostModel {

    private UUID id;

    private OrderStatus orderStatus;

    private Instant createdAt;

    private Instant updatedAt;

    private String createdBy;

    private String lastModifiedBy;
}
