package io.vextasy.ordertakingapi.model.order;

import io.vextasy.ordertakingapi.entity.OrderStatus;
import io.vextasy.ordertakingapi.validation.ValueOfEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class OrderPutModel extends OrderPostModel {

    @ValueOfEnum(enumClass = OrderStatus.class)
    @Enumerated(EnumType.STRING)
    private String orderStatus;
}
