package io.vextasy.ordertakingapi.model.order;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class OrderPostModel {

    private UUID customerId;

    @NotEmpty(message = "First name is required")
    private String firstName;

    @NotEmpty(message = "Last name is required")
    private String lastName;

    @Email(message = "Invalid email address format!")
    private String email;

    @NotEmpty(message = "Phone number is required")
    private String phoneNumber;

    @NotEmpty(message = "Billing address is required")
    private String billingAddress;

    @NotEmpty(message = "Order must contain at least one product")
    private List<UUID> productIds;

    @NotEmpty(message = "Installation address is required")
    private String installationAddress;

    @NotNull(message = "Preferred installation date is required")
    @Future(message = "Preferred installation date must be in future")
    private LocalDate preferredInstallationDate;

    @NotNull(message = "From time field is required")
    @Future(message = "From time must be in future")
    private Instant fromTime;

    @NotNull(message = "To time field is required")
    @Future(message = "To time must be in future")
    private Instant toTime;
}
