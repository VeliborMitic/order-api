package io.vextasy.ordertakingapi.model.product;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class ProductGetModel extends ProductPostModel {

    private UUID id;
}
