package io.vextasy.ordertakingapi.model.product;

import io.vextasy.ordertakingapi.validation.product.ValueOfEnumProductType;
import lombok.Getter;
import lombok.Setter;
import io.vextasy.ordertakingapi.entity.ProductType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Getter
@Setter
public class ProductPostModel {

    @NotBlank
    @ValueOfEnumProductType(enumClass = ProductType.class)
    @Enumerated(EnumType.STRING)
    private String productType;

    @NotBlank(message = "Product name is required, and must be unique")
    private String name;

    @NotNull(message = "Product price is required")
    @Positive(message = "Product price must be positive value")
    private BigDecimal price;

    private String description;
}
