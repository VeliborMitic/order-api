package io.vextasy.ordertakingapi.model.customer;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerPostModel {

    @NotBlank(message = "First name field is required!")
    private String firstName;

    @NotBlank(message = "Last name field is required!")
    private String lastName;

    @NotNull
    @Email(message = "Invalid email address format!")
    private String email;

    @NotBlank(message = "Phone number field is required!")
    private String phoneNumber;

    @NotBlank(message = "Billing address field is required!")
    private String billingAddress;
}
