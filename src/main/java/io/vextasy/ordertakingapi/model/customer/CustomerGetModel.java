package io.vextasy.ordertakingapi.model.customer;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class CustomerGetModel extends CustomerPostModel {

    private UUID id;
}
