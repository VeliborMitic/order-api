CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP TABLE IF EXISTS accounts CASCADE;
DROP TABLE IF EXISTS customers CASCADE;
DROP TABLE IF EXISTS orders_products CASCADE;
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE IF EXISTS orders CASCADE;

create table if not exists accounts
(
	id uuid default uuid_generate_v4() not null
		constraint accounts_pkey
			primary key,
	active boolean not null,
	created_at timestamp,
	created_by varchar(255),
	email varchar(255)
		constraint uk_n7ihswpy07ci568w34q0oi8he
			unique,
	last_modified_by varchar(255),
	password varchar(255),
	role varchar(255),
	updated_at timestamp,
	verified_email boolean not null
);
alter table accounts owner to postgres;


create table if not exists customers
(
	id uuid default uuid_generate_v4() not null
		constraint customers_pkey
			primary key,
	billing_address varchar(255),
	email varchar(255)
		constraint uk_rfbvkrffamfql7cjmen8v976v
			unique,
	first_name varchar(255),
	last_name varchar(255),
	phone_number varchar(255)
);
alter table customers owner to postgres;


create table if not exists orders
(
	id uuid default uuid_generate_v4() not null
		constraint orders_pkey
			primary key,
	created_at timestamp,
	created_by varchar(255),
	from_time timestamp,
	installation_address varchar(255),
	last_modified_by varchar(255),
	order_status varchar(255),
	preferred_installation_date date,
	to_time timestamp,
	updated_at timestamp,
	customer_id uuid default uuid_generate_v4() not null
		constraint fkpxtb8awmi0dk6smoh2vp1litg
			references customers
);
alter table orders owner to postgres;


create table if not exists products
(
	id uuid default uuid_generate_v4() not null
		constraint products_pkey
			primary key,
	description varchar(255),
	name varchar(255)
		constraint uk_o61fmio5yukmmiqgnxf8pnavn
			unique,
	price numeric(19,2),
	product_type varchar(255)
);
alter table products owner to postgres;


create table if not exists orders_products
(
	order_id uuid default uuid_generate_v4() not null
		constraint fke4y1sseio787e4o5hrml7omt5
			references orders,
	products_id uuid default uuid_generate_v4() not null
		constraint uk_qmviv5y7625wak8tjq4nirybh
			unique
		constraint fkqgxvu9mvqx0bv2ew776laoqvv
			references products
);

alter table orders_products owner to postgres;