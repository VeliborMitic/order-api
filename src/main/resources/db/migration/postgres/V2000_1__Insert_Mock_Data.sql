INSERT INTO accounts(id, active, created_at, created_by, email, last_modified_by, password, role, updated_at,
                     verified_email)
values ('e3fcd845-52e8-4474-b363-324b0a397c40', true, '2020-04-01 10:00:57.732000', 'admin@x_company.com',
        'admin@x_company.com', 'admin@x_company.com', '$2a$10$mYwTmebtTMXaaKf5onFPe.fflxKLw0fho/4Zu36/PkaOXA2Mrqd5e',
        'ROLE_ADMIN', '2020-04-01 10:00:57.732000', true),
       ('2818a29f-eb06-4c09-b7dd-6fd567b683c0', true, '2020-04-02 10:00:57.000000', 'admin@x_company.com',
        'erp@x_company.com', 'admin@x_company.com', '$2a$10$mYwTmebtTMXaaKf5onFPe.fflxKLw0fho/4Zu36/PkaOXA2Mrqd5e',
        'ERP_ACCOUNT', '2020-04-02 10:00:57.000000', true),
       ('cab019b7-471a-479c-b8fa-16fb9b09f7a1', true, '2020-04-02 10:00:58.000000', 'admin@x_company.com',
        'crm@x_company.com', 'admin@x_company.com', '$2a$10$mYwTmebtTMXaaKf5onFPe.fflxKLw0fho/4Zu36/PkaOXA2Mrqd5e',
        'CRM_ACCOUNT', '2020-04-02 10:00:58.000000', true),
       ('03d17574-204b-4d66-a20e-5509cba7ca3b', true, '2020-07-26 21:02:36.873000', 'admin@x_company.com',
        'radovanthethird@gmail.com', 'admin@x_company.com',
        '$2a$10$mYwTmebtTMXaaKf5onFPe.fflxKLw0fho/4Zu36/PkaOXA2Mrqd5e',
        'ERP_ACCOUNT', '2020-07-26 21:03:06.074000', true)
;

INSERT INTO products(id, description, name, price, product_type)
values ('e47bac6c-bc3f-462c-8e0f-9c760e5f400d', 'Internet - 250Mbps', '250 Mbps', '20', 'INTERNET'),
       ('dfc203e5-6272-43dd-84e6-2c37547e9897', 'Internet - 500Mbps', '500 Mbps', '30', 'INTERNET'),
       ('f840d143-e436-4d25-b5c6-d3134cd590f5', 'Internet - 1Gbps', '1 Gbps', '40', 'INTERNET'),
       ('26e8ccc5-dc03-4025-89d5-ddcb35b3c25f', 'TV - 90 Channels', '90 Ch', '15', 'TV'),
       ('1fe7fdf9-1971-46c5-b24a-7de7d53a59b3', 'TV - 140 Channels', '140 Ch', '15', 'TV'),
       ('a6cc886c-5ad2-49ad-a44a-a89c80e72ddc', 'Telephony - Free On net Calls', 'Free-on-net', '15', 'TELEPHONY'),
       ('a20eab48-67e6-4ea2-a8ad-b7d641ff58f8', 'Telephony - Unlimited Calls', 'Unlimited Calls', '25', 'TELEPHONY'),
       ('4445ae13-1727-4b3a-8b6a-4b087136245b', 'Mobile Prepaid', 'Mobile Prepaid', '0', 'MOBILE'),
       ('d9f187f8-bc38-480e-b37b-800e122f5919', 'Mobile Postpaid', 'Mobile Postpaid', '25', 'MOBILE');

INSERT INTO customers (id, billing_address, email, first_name, last_name, phone_number)
values ('9233f37d-45a8-4ba2-aeeb-a7115f47b1bd', 'Rade Koncara 48, Leskovac', 'velibor@castel.co.rs', 'Velibor', 'Mitic',
        '0652365600');

INSERT INTO orders(id, created_at, created_by, last_modified_by, from_time, installation_address,
                   preferred_installation_date, to_time, order_status, updated_at, customer_id)
values ('9e743b27-399f-42ad-bfc6-f169cf1924f1', '2020-07-05 10:00:57.732000', 'erp@x_company.com',
        'admin@x_company.com', '2020-07-30 10:00:00.000000', 'Rade Koncara 48, Leskovac', '2020-07-30',
        '2020-07-30 14:00:00.000000', 'ACCEPTED', '2020-07-05 10:00:57.732000', '9233f37d-45a8-4ba2-aeeb-a7115f47b1bd');

INSERT INTO orders_products(order_id, products_id)
values ('9e743b27-399f-42ad-bfc6-f169cf1924f1', 'e47bac6c-bc3f-462c-8e0f-9c760e5f400d'),
       ('9e743b27-399f-42ad-bfc6-f169cf1924f1', '1fe7fdf9-1971-46c5-b24a-7de7d53a59b3');