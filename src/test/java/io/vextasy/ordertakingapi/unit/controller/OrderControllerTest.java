package io.vextasy.ordertakingapi.unit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vextasy.ordertakingapi.entity.Customer;
import io.vextasy.ordertakingapi.entity.Order;
import io.vextasy.ordertakingapi.entity.OrderStatus;
import io.vextasy.ordertakingapi.entity.Product;
import io.vextasy.ordertakingapi.entity.ProductType;
import io.vextasy.ordertakingapi.model.order.OrderPostModel;
import io.vextasy.ordertakingapi.model.order.OrderPutModel;
import io.vextasy.ordertakingapi.security.UserCredentials;
import io.vextasy.ordertakingapi.service.impl.OrderServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class OrderControllerTest {

    private static final Logger LOGGER = LogManager.getLogger(OrderControllerTest.class);

    private static final String BASE_URI = "/api/v1/orders";
    private static final String SLASH_ID = "/{id}";
    private static final String INSTALLATION_ADDRESS = "111 Some Street, Some Town";
    private static final LocalDate PREFERRED_DATE = LocalDate.of(2020, 8, 1);
    private static final Instant FROM_TIME = Instant.parse("2020-08-01T10:00:00.000000Z");
    private static final Instant TO_TIME = Instant.parse("2020-08-01T16:00:00.000000Z");
    private static final String EMAIL = "customer@x_company.com";
    private static final String FIRST_NAME = "Bob";
    private static final String LAST_NAME = "Builder";
    private static final String BILLING_ADDRESS = "111 Some Street, Some Town";
    private static final String PHONE_NUMBER = "+381652365600";
    private static final String PRODUCT_NAME = "250 Mbs";
    private static final ProductType PRODUCT_TYPE = ProductType.INTERNET;
    private static final BigDecimal PRODUCT_PRICE = BigDecimal.valueOf(25);
    private static final String PRODUCT_DESCRIPTION = "Internet - 250 Mbs";

    @MockBean
    private OrderServiceImpl mockOrderService;

    @Autowired
    private MockMvc mockMvc;

    private String token;

    private OrderPostModel postModel;

    private OrderPutModel putModel;

    UserCredentials adminCredentials;
    UserCredentials erpCredentials;
    UserCredentials crmCredentials;

    private Order order;

    @BeforeEach
    void initData() {

        adminCredentials = new UserCredentials();
        adminCredentials.setEmail("admin@x_company.com");
        adminCredentials.setPassword("123456");

        erpCredentials = new UserCredentials();
        erpCredentials.setEmail("erp@x_company.com");
        erpCredentials.setPassword("123456");

        crmCredentials = new UserCredentials();
        crmCredentials.setEmail("crm@x_company.com");
        crmCredentials.setPassword("123456");

        Customer customer = new Customer();
        customer.setId(UUID.fromString("c8051111-b882-4cbc-9020-be49dbc3778a"));
        customer.setFirstName(FIRST_NAME);
        customer.setLastName(LAST_NAME);
        customer.setEmail(EMAIL);
        customer.setBillingAddress(BILLING_ADDRESS);
        customer.setPhoneNumber(PHONE_NUMBER);

        List<Product> products = new ArrayList<>();

        Product product1 = new Product();
        product1.setId(UUID.fromString("1e36151e-4ac9-4a4d-a77d-dc90e5428ea1"));
        product1.setName(PRODUCT_NAME);
        product1.setProductType(PRODUCT_TYPE);
        product1.setPrice(PRODUCT_PRICE);
        product1.setDescription(PRODUCT_DESCRIPTION);
        products.add(product1);

        Product product2 = new Product();
        product2.setId(UUID.fromString("4f14bcbe-e222-413d-8794-526a84bbd910"));
        product2.setName(PRODUCT_NAME);
        product2.setProductType(PRODUCT_TYPE);
        product2.setPrice(PRODUCT_PRICE);
        product2.setDescription(PRODUCT_DESCRIPTION);
        products.add(product2);

        order = new Order();
        order.setId(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));
        order.setCustomer(customer);
        order.setProducts(products);
        order.setOrderStatus(OrderStatus.ACCEPTED);
        order.setInstallationAddress(INSTALLATION_ADDRESS);
        order.setPreferredInstallationDate(PREFERRED_DATE);
        order.setFromTime(FROM_TIME);
        order.setToTime(TO_TIME);

        List<UUID> productIds = new ArrayList<>();
        productIds.add(UUID.fromString("1e36151e-4ac9-4a4d-a77d-dc90e5428ea1"));
        productIds.add(UUID.fromString("4f14bcbe-e222-413d-8794-526a84bbd910"));

        postModel = new OrderPostModel();
        postModel.setFirstName(FIRST_NAME);
        postModel.setLastName(LAST_NAME);
        postModel.setEmail(EMAIL);
        postModel.setBillingAddress(BILLING_ADDRESS);
        postModel.setPhoneNumber(PHONE_NUMBER);
        postModel.setProductIds(productIds);
        postModel.setInstallationAddress(INSTALLATION_ADDRESS);
        postModel.setPreferredInstallationDate(PREFERRED_DATE);
        postModel.setFromTime(FROM_TIME);
        postModel.setToTime(TO_TIME);

        putModel = new OrderPutModel();
        putModel.setFirstName(FIRST_NAME);
        putModel.setLastName(LAST_NAME);
        putModel.setEmail(EMAIL);
        putModel.setBillingAddress(BILLING_ADDRESS);
        putModel.setPhoneNumber(PHONE_NUMBER);
        putModel.setProductIds(productIds);
        putModel.setInstallationAddress(INSTALLATION_ADDRESS);
        putModel.setPreferredInstallationDate(PREFERRED_DATE);
        putModel.setFromTime(FROM_TIME);
        putModel.setToTime(TO_TIME);
        putModel.setOrderStatus(String.valueOf(OrderStatus.ACCEPTED));
    }

    void authenticate(UserCredentials userCredentials) {

        MvcResult mvcResult = null;
        try {
            mvcResult = mockMvc.perform(post("/api/v1/auth")
                    .contentType(APPLICATION_JSON)
                    .content(String.valueOf(asJsonString(userCredentials))))
                    .andReturn();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        if (mvcResult != null) {
            token = mvcResult.getResponse().getHeader(HttpHeaders.AUTHORIZATION);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when Order exists")
    void testGetOrderOk() {

        authenticate(adminCredentials);
        doReturn(Optional.of(order)).when(mockOrderService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(jsonPath("id").value("e3fcd845-52e8-4474-b363-324b0a397c40"));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '404 Not Found' when Order does not exist")
    void testGetOrderNonExistingUuid() {

        authenticate(adminCredentials);
        doReturn(Optional.empty()).when(mockOrderService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when retrieving all Orders")
    void testGetOrdersOk() {

        authenticate(adminCredentials);
        try {
            mockMvc.perform(get(BASE_URI)
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '201 Created' when Order is created")
    void testCreateOrderCreated() {

        authenticate(erpCredentials);

        doReturn(Optional.of(order)).when(mockOrderService).create(any());

        try {
            mockMvc.perform(post(BASE_URI)
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(postModel)))

                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(jsonPath("id").value("e3fcd845-52e8-4474-b363-324b0a397c40"));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @ParameterizedTest
    @MethodSource("dataProvider")
    @DisplayName("Return '400 Bad Request' when create request does not contain valid body")
    void testCreateOrderBadRequest(OrderPostModel orderPostModel) {

        authenticate(erpCredentials);

        try {
            mockMvc.perform(post(BASE_URI)
                    .content(asJsonString(orderPostModel))
                    .header(AUTHORIZATION, token)
                    .header(CONTENT_TYPE, "application/json"))
                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private static Stream<Arguments> dataProvider() {

        List<UUID> productIds = new ArrayList<>();
        productIds.add(UUID.fromString("1e36151e-4ac9-4a4d-a77d-dc90e5428ea1"));
        productIds.add(UUID.fromString("4f14bcbe-e222-413d-8794-526a84bbd910"));

        OrderPostModel orderModelWithoutFirstName = new OrderPostModel();
        orderModelWithoutFirstName.setLastName(LAST_NAME);
        orderModelWithoutFirstName.setEmail(EMAIL);
        orderModelWithoutFirstName.setPhoneNumber(PHONE_NUMBER);
        orderModelWithoutFirstName.setBillingAddress(BILLING_ADDRESS);
        orderModelWithoutFirstName.setProductIds(productIds);
        orderModelWithoutFirstName.setInstallationAddress(INSTALLATION_ADDRESS);
        orderModelWithoutFirstName.setPreferredInstallationDate(PREFERRED_DATE);
        orderModelWithoutFirstName.setFromTime(FROM_TIME);
        orderModelWithoutFirstName.setToTime(TO_TIME);

        OrderPostModel orderModelWithoutLastName = new OrderPostModel();
        orderModelWithoutLastName.setFirstName(FIRST_NAME);
        orderModelWithoutLastName.setEmail(EMAIL);
        orderModelWithoutLastName.setPhoneNumber(PHONE_NUMBER);
        orderModelWithoutLastName.setBillingAddress(BILLING_ADDRESS);
        orderModelWithoutLastName.setProductIds(productIds);
        orderModelWithoutLastName.setInstallationAddress(INSTALLATION_ADDRESS);
        orderModelWithoutLastName.setPreferredInstallationDate(PREFERRED_DATE);
        orderModelWithoutLastName.setFromTime(FROM_TIME);
        orderModelWithoutLastName.setToTime(TO_TIME);

        OrderPostModel orderModelWithoutEmail = new OrderPostModel();
        orderModelWithoutEmail.setFirstName(FIRST_NAME);
        orderModelWithoutEmail.setLastName(LAST_NAME);
        orderModelWithoutEmail.setPhoneNumber(PHONE_NUMBER);
        orderModelWithoutEmail.setBillingAddress(BILLING_ADDRESS);
        orderModelWithoutEmail.setProductIds(productIds);
        orderModelWithoutEmail.setInstallationAddress(INSTALLATION_ADDRESS);
        orderModelWithoutEmail.setPreferredInstallationDate(PREFERRED_DATE);
        orderModelWithoutEmail.setFromTime(FROM_TIME);
        orderModelWithoutEmail.setToTime(TO_TIME);

        OrderPostModel orderModelWithoutPhoneNumber = new OrderPostModel();
        orderModelWithoutPhoneNumber.setFirstName(FIRST_NAME);
        orderModelWithoutPhoneNumber.setLastName(LAST_NAME);
        orderModelWithoutPhoneNumber.setEmail(EMAIL);
        orderModelWithoutPhoneNumber.setBillingAddress(BILLING_ADDRESS);
        orderModelWithoutPhoneNumber.setProductIds(productIds);
        orderModelWithoutPhoneNumber.setInstallationAddress(INSTALLATION_ADDRESS);
        orderModelWithoutPhoneNumber.setPreferredInstallationDate(PREFERRED_DATE);
        orderModelWithoutPhoneNumber.setFromTime(FROM_TIME);
        orderModelWithoutPhoneNumber.setToTime(TO_TIME);

        OrderPostModel orderModelWithoutBillingAddress = new OrderPostModel();
        orderModelWithoutBillingAddress.setFirstName(FIRST_NAME);
        orderModelWithoutBillingAddress.setLastName(LAST_NAME);
        orderModelWithoutBillingAddress.setEmail(EMAIL);
        orderModelWithoutBillingAddress.setPhoneNumber(PHONE_NUMBER);
        orderModelWithoutBillingAddress.setProductIds(productIds);
        orderModelWithoutBillingAddress.setInstallationAddress(INSTALLATION_ADDRESS);
        orderModelWithoutBillingAddress.setPreferredInstallationDate(PREFERRED_DATE);
        orderModelWithoutBillingAddress.setFromTime(FROM_TIME);
        orderModelWithoutBillingAddress.setToTime(TO_TIME);

        OrderPostModel orderModelWithoutProductIds = new OrderPostModel();
        orderModelWithoutProductIds.setFirstName(FIRST_NAME);
        orderModelWithoutProductIds.setLastName(LAST_NAME);
        orderModelWithoutProductIds.setEmail(EMAIL);
        orderModelWithoutProductIds.setPhoneNumber(PHONE_NUMBER);
        orderModelWithoutProductIds.setBillingAddress(BILLING_ADDRESS);
        orderModelWithoutProductIds.setInstallationAddress(INSTALLATION_ADDRESS);
        orderModelWithoutProductIds.setPreferredInstallationDate(PREFERRED_DATE);
        orderModelWithoutProductIds.setFromTime(FROM_TIME);
        orderModelWithoutProductIds.setToTime(TO_TIME);

        OrderPostModel orderModelWithoutInstallationAddress = new OrderPostModel();
        orderModelWithoutInstallationAddress.setFirstName(FIRST_NAME);
        orderModelWithoutInstallationAddress.setLastName(LAST_NAME);
        orderModelWithoutInstallationAddress.setEmail(EMAIL);
        orderModelWithoutInstallationAddress.setPhoneNumber(PHONE_NUMBER);
        orderModelWithoutInstallationAddress.setBillingAddress(BILLING_ADDRESS);
        orderModelWithoutInstallationAddress.setProductIds(productIds);
        orderModelWithoutInstallationAddress.setPreferredInstallationDate(PREFERRED_DATE);
        orderModelWithoutInstallationAddress.setFromTime(FROM_TIME);
        orderModelWithoutInstallationAddress.setToTime(TO_TIME);

        OrderPostModel orderModelWithoutPreferredDate = new OrderPostModel();
        orderModelWithoutPreferredDate.setFirstName(FIRST_NAME);
        orderModelWithoutPreferredDate.setLastName(LAST_NAME);
        orderModelWithoutPreferredDate.setEmail(EMAIL);
        orderModelWithoutPreferredDate.setPhoneNumber(PHONE_NUMBER);
        orderModelWithoutPreferredDate.setBillingAddress(BILLING_ADDRESS);
        orderModelWithoutPreferredDate.setProductIds(productIds);
        orderModelWithoutPreferredDate.setInstallationAddress(INSTALLATION_ADDRESS);
        orderModelWithoutPreferredDate.setFromTime(FROM_TIME);
        orderModelWithoutPreferredDate.setToTime(TO_TIME);

        OrderPostModel orderModelWithoutFromTime = new OrderPostModel();
        orderModelWithoutFromTime.setFirstName(FIRST_NAME);
        orderModelWithoutFromTime.setLastName(LAST_NAME);
        orderModelWithoutFromTime.setEmail(EMAIL);
        orderModelWithoutFromTime.setPhoneNumber(PHONE_NUMBER);
        orderModelWithoutFromTime.setBillingAddress(BILLING_ADDRESS);
        orderModelWithoutFromTime.setProductIds(productIds);
        orderModelWithoutFromTime.setInstallationAddress(INSTALLATION_ADDRESS);
        orderModelWithoutFromTime.setPreferredInstallationDate(PREFERRED_DATE);
        orderModelWithoutFromTime.setToTime(TO_TIME);

        OrderPostModel orderModelWithoutToTime = new OrderPostModel();
        orderModelWithoutToTime.setFirstName(FIRST_NAME);
        orderModelWithoutToTime.setLastName(LAST_NAME);
        orderModelWithoutToTime.setEmail(EMAIL);
        orderModelWithoutToTime.setPhoneNumber(PHONE_NUMBER);
        orderModelWithoutToTime.setBillingAddress(BILLING_ADDRESS);
        orderModelWithoutToTime.setProductIds(productIds);
        orderModelWithoutToTime.setInstallationAddress(INSTALLATION_ADDRESS);
        orderModelWithoutToTime.setPreferredInstallationDate(PREFERRED_DATE);
        orderModelWithoutToTime.setFromTime(FROM_TIME);

        return Stream.of(
                Arguments.of(orderModelWithoutFirstName),
                Arguments.of(orderModelWithoutLastName),
                Arguments.of(orderModelWithoutEmail),
                Arguments.of(orderModelWithoutPhoneNumber),
                Arguments.of(orderModelWithoutBillingAddress),
                Arguments.of(orderModelWithoutProductIds),
                Arguments.of(orderModelWithoutInstallationAddress),
                Arguments.of(orderModelWithoutPreferredDate),
                Arguments.of(orderModelWithoutFromTime),
                Arguments.of(orderModelWithoutToTime)
        );
    }

    @Test
    @DisplayName("Return '200 OK' when Order is updated successfully")
    void testUpdateOrderOk() {

        authenticate(crmCredentials);

        doReturn(Optional.of(order)).when(mockOrderService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));
        doReturn(Optional.of(order)).when(mockOrderService).update(any());

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, "e3fcd845-52e8-4474-b363-324b0a397c40")
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(putModel)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 Bad Request' when provided id is invalid")
    void testUpdateOrderNotFound() {

        authenticate(crmCredentials);
        doReturn(Optional.empty()).when(mockOrderService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(putModel)))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings("squid:S00112")
    private static String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
