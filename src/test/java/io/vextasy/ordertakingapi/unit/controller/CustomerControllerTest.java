package io.vextasy.ordertakingapi.unit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vextasy.ordertakingapi.entity.Customer;
import io.vextasy.ordertakingapi.model.customer.CustomerPostModel;
import io.vextasy.ordertakingapi.security.UserCredentials;
import io.vextasy.ordertakingapi.service.impl.CustomerServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class CustomerControllerTest {

    private static final Logger LOGGER = LogManager.getLogger(CustomerControllerTest.class);

    private static final String BASE_URI = "/api/v1/customers";
    private static final String SLASH_ID = "/{id}";
    private static final String EMAIL = "customer@x_company.com";
    private static final String FIRST_NAME = "Bob";
    private static final String LAST_NAME = "Builder";
    private static final String BILLING_ADDRESS = "111 Some Street, Some Town";
    private static final String PHONE_NUMBER = "+381652365600";

    @MockBean
    private CustomerServiceImpl mockCustomerService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MockMvc mockMvc;

    private String token;

    private CustomerPostModel postModel;

    private Customer customer;

    @BeforeEach
    void initData() {
        customer = new Customer();
        customer.setId(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));
        customer.setFirstName(FIRST_NAME);
        customer.setLastName(LAST_NAME);
        customer.setEmail(EMAIL);
        customer.setBillingAddress(BILLING_ADDRESS);
        customer.setPhoneNumber(PHONE_NUMBER);

        postModel = new CustomerPostModel();
        postModel.setFirstName(FIRST_NAME);
        postModel.setLastName(LAST_NAME);
        postModel.setEmail(EMAIL);
        postModel.setBillingAddress(BILLING_ADDRESS);
        postModel.setPhoneNumber(PHONE_NUMBER);
    }

    @BeforeAll
    void authenticate() {
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("admin@x_company.com");
        userCredentials.setPassword("123456");

        MvcResult mvcResult = null;
        try {
            mvcResult = mockMvc.perform(post("/api/v1/auth")
                    .contentType(APPLICATION_JSON)
                    .content(String.valueOf(asJsonString(userCredentials))))
                    .andReturn();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        if (mvcResult != null) {
            token = mvcResult.getResponse().getHeader(HttpHeaders.AUTHORIZATION);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when Customer exists")
    void testGetCustomerOk() {
        doReturn(Optional.of(customer)).when(mockCustomerService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(jsonPath("id").value("e3fcd845-52e8-4474-b363-324b0a397c40"));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '404 Not Found' when Customer does not exist")
    void testGetCustomerNonExistingUuid() {
        doReturn(Optional.empty()).when(mockCustomerService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when retrieving all Customers")
    void testGetCustomersOk() {
        try {
            mockMvc.perform(get(BASE_URI)
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '201 Created' when Customer is created")
    void testCreateCustomerCreated() {

        Customer mapped = modelMapper.map(postModel, Customer.class);
        mapped.setId(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        doReturn(Optional.of(mapped)).when(mockCustomerService).create(any());

        try {
            mockMvc.perform(post(BASE_URI)
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(postModel)))

                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(jsonPath("id").value("e3fcd845-52e8-4474-b363-324b0a397c40"));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '409 Conflict' on attempting to create an Customer with existing email")
    void testCreateCustomerConflict() {
        Customer mapped = modelMapper.map(postModel, Customer.class);

        doReturn(Optional.of(mapped)).when(mockCustomerService).get(mapped.getId());

        try {
            mockMvc.perform(post(BASE_URI)
                    .content(asJsonString(postModel))
                    .header(AUTHORIZATION, token)
                    .header(CONTENT_TYPE, "application/json"))

                    .andExpect(status().isConflict());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @ParameterizedTest
    @MethodSource("dataProvider")
    @DisplayName("Return '400 Bad Request' when create request does not contain valid body")
    void testCreateCustomerBadRequest(CustomerPostModel customerPostModel) {

        try {
            mockMvc.perform(post(BASE_URI)
                    .content(asJsonString(customerPostModel))
                    .header(AUTHORIZATION, token)
                    .header(CONTENT_TYPE, "application/json"))
                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private static Stream<Arguments> dataProvider() {
        CustomerPostModel customerWithoutEmail = new CustomerPostModel();
        customerWithoutEmail.setFirstName(FIRST_NAME);
        customerWithoutEmail.setLastName(LAST_NAME);
        customerWithoutEmail.setBillingAddress(BILLING_ADDRESS);
        customerWithoutEmail.setPhoneNumber(PHONE_NUMBER);

        CustomerPostModel customerWithoutFirstName = new CustomerPostModel();
        customerWithoutFirstName.setEmail(EMAIL);
        customerWithoutFirstName.setLastName(LAST_NAME);
        customerWithoutFirstName.setBillingAddress(BILLING_ADDRESS);
        customerWithoutFirstName.setPhoneNumber(PHONE_NUMBER);

        CustomerPostModel customerWithoutLastName = new CustomerPostModel();
        customerWithoutLastName.setEmail(EMAIL);
        customerWithoutLastName.setFirstName(FIRST_NAME);
        customerWithoutLastName.setBillingAddress(BILLING_ADDRESS);
        customerWithoutLastName.setPhoneNumber(PHONE_NUMBER);

        CustomerPostModel customerWithoutBillingAddress = new CustomerPostModel();
        customerWithoutBillingAddress.setEmail(EMAIL);
        customerWithoutBillingAddress.setFirstName(FIRST_NAME);
        customerWithoutBillingAddress.setLastName(LAST_NAME);
        customerWithoutBillingAddress.setPhoneNumber(PHONE_NUMBER);

        CustomerPostModel customerWithoutPhoneNumber = new CustomerPostModel();
        customerWithoutPhoneNumber.setEmail(EMAIL);
        customerWithoutPhoneNumber.setFirstName(FIRST_NAME);
        customerWithoutPhoneNumber.setLastName(LAST_NAME);
        customerWithoutPhoneNumber.setBillingAddress(BILLING_ADDRESS);

        return Stream.of(
                Arguments.of(customerWithoutEmail),
                Arguments.of(customerWithoutFirstName),
                Arguments.of(customerWithoutLastName),
                Arguments.of(customerWithoutBillingAddress),
                Arguments.of(customerWithoutPhoneNumber)
        );
    }

    @Test
    @DisplayName("Return '200 OK' when Customer is updated successfully")
    void testUpdateCustomerOk() {
        Customer mapped = modelMapper.map(postModel, Customer.class);
        mapped.setId(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        doReturn(Optional.of(mapped)).when(mockCustomerService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));
        doReturn(Optional.of(mapped)).when(mockCustomerService).update(any());

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, "e3fcd845-52e8-4474-b363-324b0a397c40")
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(postModel)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 Bad Request' when provided id is invalid")
    void testUpdateCustomerNotFound() {
        doReturn(Optional.empty()).when(mockCustomerService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(postModel)))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings("squid:S00112")
    private static String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
