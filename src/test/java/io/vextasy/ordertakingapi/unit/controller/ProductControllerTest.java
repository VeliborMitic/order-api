package io.vextasy.ordertakingapi.unit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vextasy.ordertakingapi.entity.Product;
import io.vextasy.ordertakingapi.entity.ProductType;
import io.vextasy.ordertakingapi.model.product.ProductPostModel;
import io.vextasy.ordertakingapi.security.UserCredentials;
import io.vextasy.ordertakingapi.service.impl.ProductServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.provider.Arguments;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class ProductControllerTest {

    private static final Logger LOGGER = LogManager.getLogger(ProductControllerTest.class);

    private static final String BASE_URI = "/api/v1/products";
    private static final String SLASH_ID = "/{id}";
    private static final String PRODUCT_NAME = "250 Mbs";
    private static final ProductType PRODUCT_TYPE = ProductType.INTERNET;
    private static final BigDecimal PRODUCT_PRICE = BigDecimal.valueOf(25);
    private static final String PRODUCT_DESCRIPTION = "Internet - 250 Mbs";

    @MockBean
    private ProductServiceImpl mockProductService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MockMvc mockMvc;

    private String token;

    private ProductPostModel postModel;

    private Product product;

    @BeforeEach
    void initData() {
        product = new Product();
        product.setId(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));
        product.setName(PRODUCT_NAME);
        product.setProductType(PRODUCT_TYPE);
        product.setPrice(PRODUCT_PRICE);
        product.setDescription(PRODUCT_DESCRIPTION);

        postModel = new ProductPostModel();
        postModel.setName(PRODUCT_NAME);
        postModel.setProductType("Internet");
        postModel.setPrice(PRODUCT_PRICE);
        postModel.setDescription(PRODUCT_DESCRIPTION);
    }

    @BeforeAll
    void authenticate() {
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("admin@x_company.com");
        userCredentials.setPassword("123456");

        MvcResult mvcResult = null;
        try {
            mvcResult = mockMvc.perform(post("/api/v1/auth")
                    .contentType(APPLICATION_JSON)
                    .content(String.valueOf(asJsonString(userCredentials))))
                    .andReturn();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        if (mvcResult != null) {
            token = mvcResult.getResponse().getHeader(HttpHeaders.AUTHORIZATION);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when Product exists")
    void testGetProductOk() {
        doReturn(Optional.of(product)).when(mockProductService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(jsonPath("id").value("e3fcd845-52e8-4474-b363-324b0a397c40"));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '404 Not Found' when Product does not exist")
    void testGetProductNonExistingUuid() {
        doReturn(Optional.empty()).when(mockProductService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when retrieving all Products")
    void testGetProductsOk() {
        try {
            mockMvc.perform(get(BASE_URI)
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '201 Created' when Product is created")
    void testCreateProductCreated() {

        Product mapped = modelMapper.map(postModel, Product.class);
        mapped.setId(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        doReturn(Optional.of(mapped)).when(mockProductService).create(any());

        try {
            mockMvc.perform(post(BASE_URI)
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(postModel)))

                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(jsonPath("id").value("e3fcd845-52e8-4474-b363-324b0a397c40"));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private static Stream<Arguments> dataProvider() {

        ProductPostModel productWithoutProductType = new ProductPostModel();
        productWithoutProductType.setName(PRODUCT_NAME);
        productWithoutProductType.setDescription(PRODUCT_DESCRIPTION);
        productWithoutProductType.setPrice(PRODUCT_PRICE);

        ProductPostModel productWithoutName = new ProductPostModel();
        productWithoutName.setProductType(String.valueOf(PRODUCT_TYPE));
        productWithoutName.setDescription(PRODUCT_DESCRIPTION);
        productWithoutName.setPrice(PRODUCT_PRICE);

        ProductPostModel productWithoutPrice = new ProductPostModel();
        productWithoutName.setProductType(String.valueOf(PRODUCT_TYPE));
        productWithoutName.setName(PRODUCT_NAME);
        productWithoutName.setDescription(PRODUCT_DESCRIPTION);

        return Stream.of(
                Arguments.of(productWithoutProductType),
                Arguments.of(productWithoutName),
                Arguments.of(productWithoutPrice)
        );
    }

    @Test
    @DisplayName("Return '200 OK' when Product is updated successfully")
    void testUpdateProductOk() {
        Product mapped = modelMapper.map(postModel, Product.class);
        mapped.setId(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        doReturn(Optional.of(mapped)).when(mockProductService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));
        doReturn(Optional.of(mapped)).when(mockProductService).update(any());

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, "e3fcd845-52e8-4474-b363-324b0a397c40")
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(postModel)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 Bad Request' when provided id is invalid")
    void testUpdateProductNotFound() {
        doReturn(Optional.empty()).when(mockProductService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(postModel)))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '204 No Content' when Product is successfully deleted")
    void testDeleteProductNoContent() {
        doReturn(Optional.of(product)).when(mockProductService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(delete(BASE_URI + SLASH_ID, "e3fcd845-52e8-4474-b363-324b0a397c40")
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isNoContent());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 Bad Request' when provided id is invalid")
    void testDeleteProductNotFound() {
        doReturn(Optional.empty()).when(mockProductService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));
        try {
            mockMvc.perform(delete(BASE_URI + SLASH_ID, "aaaaaaaaaa")
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings("squid:S00112")
    private static String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
