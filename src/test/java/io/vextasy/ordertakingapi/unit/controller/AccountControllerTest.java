package io.vextasy.ordertakingapi.unit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vextasy.ordertakingapi.entity.Account;
import io.vextasy.ordertakingapi.entity.Role;
import io.vextasy.ordertakingapi.model.account.AccountPostModel;
import io.vextasy.ordertakingapi.security.UserCredentials;
import io.vextasy.ordertakingapi.service.impl.AccountServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class AccountControllerTest {

    private static final Logger LOGGER = LogManager.getLogger(AccountControllerTest.class);

    private static final String BASE_URI = "/api/v1/accounts";
    private static final String SLASH_ID = "/{id}";
    private static final String EMAIL = "aaaaaaaa@x_company.com";
    private static final String PASSWORD = "123456";

    @MockBean
    private AccountServiceImpl mockAccountService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MockMvc mockMvc;

    private String token;

    private AccountPostModel postModel;

    private Account account;

    @BeforeEach
    void initData() {
        account = new Account();
        account.setId(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));
        account.setEmail("admin@x_company.com");
        account.setVerifiedEmail(true);
        account.setPassword("123456");
        account.setRole(Role.ROLE_ADMIN);
        account.setActive(true);

        postModel = new AccountPostModel();
        postModel.setEmail("bob@the-builder.com");
        postModel.setPassword("123456");
        postModel.setRole(String.valueOf(Role.ROLE_ADMIN));
        postModel.setActive(true);
    }

    @BeforeAll
    void authenticate() {
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail("admin@x_company.com");
        userCredentials.setPassword("123456");

        MvcResult mvcResult = null;
        try {
            mvcResult = mockMvc.perform(post("/api/v1/auth")
                    .contentType(APPLICATION_JSON)
                    .content(String.valueOf(asJsonString(userCredentials))))
                    .andReturn();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        if (mvcResult != null) {
            token = mvcResult.getResponse().getHeader(HttpHeaders.AUTHORIZATION);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when Account exists")
    void testGetAccountOk() {
        doReturn(Optional.of(account)).when(mockAccountService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(jsonPath("id").value("e3fcd845-52e8-4474-b363-324b0a397c40"));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '404 Not Found' when Account does not exist")
    void testGetAccountNonExistingUuid() {
        doReturn(Optional.empty()).when(mockAccountService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(get(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '200 OK' when retrieving all Accounts")
    void testGetAccountsOk() {
        try {
            mockMvc.perform(get(BASE_URI)
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '201 Created' when Account is created")
    void testCreateAccountCreated() {

        Account mapped = modelMapper.map(postModel, Account.class);
        mapped.setId(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        doReturn(Optional.of(mapped)).when(mockAccountService).create(any());

        try {
            mockMvc.perform(post(BASE_URI)
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(postModel)))

                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(jsonPath("id").value("e3fcd845-52e8-4474-b363-324b0a397c40"));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '409 Conflict' on attempting to create an Account with existing email")
    void testCreateAccountConflict() {
        Account mapped = modelMapper.map(postModel, Account.class);

        doReturn(Optional.of(mapped)).when(mockAccountService).get(mapped.getId());

        try {
            mockMvc.perform(post(BASE_URI)
                    .content(asJsonString(postModel))
                    .header(AUTHORIZATION, token)
                    .header(CONTENT_TYPE, "application/json"))

                    .andExpect(status().isConflict());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @ParameterizedTest
    @MethodSource("dataProvider")
    @DisplayName("Return '400 Bad Request' when create request does not contain valid body")
    void testCreateAccountBadRequest(AccountPostModel accountPostModel) {

        try {
            mockMvc.perform(post(BASE_URI)
                    .content(asJsonString(accountPostModel))
                    .header(AUTHORIZATION, token)
                    .header(CONTENT_TYPE, "application/json"))
                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private static Stream<Arguments> dataProvider() {
        AccountPostModel accountWithoutEmail = new AccountPostModel();
        accountWithoutEmail.setActive(true);
        accountWithoutEmail.setPassword(PASSWORD);
        accountWithoutEmail.setRole(String.valueOf(Role.ROLE_ADMIN));

        AccountPostModel accountWithoutRole = new AccountPostModel();
        accountWithoutRole.setEmail(EMAIL);
        accountWithoutRole.setPassword(PASSWORD);
        accountWithoutRole.setActive(true);

        AccountPostModel accountWithoutPassword = new AccountPostModel();
        accountWithoutPassword.setEmail(EMAIL);
        accountWithoutPassword.setActive(true);
        accountWithoutPassword.setRole(String.valueOf(Role.ROLE_ADMIN));

        return Stream.of(
                Arguments.of(accountWithoutEmail),
                Arguments.of(accountWithoutRole),
                Arguments.of(accountWithoutPassword)
        );
    }

    @Test
    @DisplayName("Return '200 OK' when Account is updated successfully")
    void testUpdateAccountOk() {
        Account mapped = modelMapper.map(postModel, Account.class);
        mapped.setId(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        doReturn(Optional.of(mapped)).when(mockAccountService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));
        doReturn(Optional.of(mapped)).when(mockAccountService).update(any());

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, "e3fcd845-52e8-4474-b363-324b0a397c40")
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(postModel)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 Bad Request' when provided id is invalid")
    void testUpdateAccountNotFound() {
        doReturn(Optional.empty()).when(mockAccountService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(put(BASE_URI + SLASH_ID, UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"))
                    .header(AUTHORIZATION, token)
                    .contentType(APPLICATION_JSON)
                    .content(asJsonString(postModel)))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '204 No Content' when Account is successfully deleted")
    void testDeleteAccountNoContent() {
        doReturn(Optional.of(account)).when(mockAccountService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));

        try {
            mockMvc.perform(delete(BASE_URI + SLASH_ID, "e3fcd845-52e8-4474-b363-324b0a397c40")
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isNoContent());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    @DisplayName("Return '400 Bad Request' when provided id is invalid")
    void testDeleteAccountNotFound() {
        doReturn(Optional.empty()).when(mockAccountService).get(UUID.fromString("e3fcd845-52e8-4474-b363-324b0a397c40"));
        try {
            mockMvc.perform(delete(BASE_URI + SLASH_ID, "aaaaaaaaaa")
                    .header(AUTHORIZATION, token))

                    .andExpect(status().isBadRequest());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings("squid:S00112")
    private static String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
