package io.vextasy.ordertakingapi.unit.service;

import io.vextasy.ordertakingapi.entity.Customer;
import io.vextasy.ordertakingapi.entity.Order;
import io.vextasy.ordertakingapi.entity.OrderStatus;
import io.vextasy.ordertakingapi.entity.Product;
import io.vextasy.ordertakingapi.entity.ProductType;
import io.vextasy.ordertakingapi.repository.CustomerRepository;
import io.vextasy.ordertakingapi.repository.OrderRepository;
import io.vextasy.ordertakingapi.repository.ProductRepository;
import io.vextasy.ordertakingapi.service.impl.CustomerServiceImpl;
import io.vextasy.ordertakingapi.service.impl.OrderServiceImpl;
import io.vextasy.ordertakingapi.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class OrderServiceTest {

    @Mock
    private OrderRepository mockOrderRepository;

    @MockBean
    private OrderServiceImpl orderServiceUnderTest;

    private final Order order;

    OrderServiceTest(){
        Customer customer = new Customer();
        customer.setFirstName("Bob");
        customer.setLastName("Builder");
        customer.setEmail("bob@the-builder.com");
        customer.setPhoneNumber("123456789");
        customer.setBillingAddress("Some Street 111, SomeTown");

        List<Product> products = new ArrayList<>();

        order = new Order();
        order.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));
        order.setCustomer(customer);
        order.setPreferredInstallationDate(LocalDate.parse("2020-07-30"));
        order.setFromTime(Instant.parse("2020-07-30T10:00:00.000000Z"));
        order.setToTime(Instant.parse("2020-07-30T15:00:00.000000Z"));
        order.setInstallationAddress("111 Some Street, Somewhere");
        order.setProducts(products);
    }

    @BeforeEach
    void setUp(){
        initMocks(this);
        orderServiceUnderTest = new OrderServiceImpl(mockOrderRepository);

        doReturn(Optional.of(order)).when(mockOrderRepository).findById(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));
    }

    @Test
    void testGet() {
        final Optional<Order> optionalResult = orderServiceUnderTest.get(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));

        optionalResult.ifPresent(a -> {
            assertEquals(a, optionalResult.get());
            assertEquals(a.getCustomer(), optionalResult.get().getCustomer());
            assertEquals(a.getProducts(), optionalResult.get().getProducts());
            assertEquals(a.getInstallationAddress(), optionalResult.get().getInstallationAddress());
            assertEquals(a.getClass(), optionalResult.get().getClass());
        });
    }

    @Test
    void testGetAll() {
        final List<Order> orders = new ArrayList<>();
        Order order2 = new Order();
        order2.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
        order2.setOrderStatus(OrderStatus.ACCEPTED);

        orders.add(order);
        orders.add(order2);
        doReturn(orders).when(mockOrderRepository).findAll();

        final List<Order> results = orderServiceUnderTest.get();

        assertFalse(results.isEmpty());
        assertEquals(results.get(0), orders.get(0));
        assertEquals(2, results.size());
        assertEquals(orders.getClass(), results.getClass());
        assertEquals(orders, results);
    }

    @Test
    void testCreate() {
        doReturn(order).when(mockOrderRepository).save(order);

        final Optional<Order> result = orderServiceUnderTest.create(order);

        result.ifPresent(a -> {
            assertEquals(a.getId(), result.get().getId());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testUpdate() {
        Order updated = new Order();
        updated.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
        doReturn(updated).when(mockOrderRepository).save(order);

        final Optional<Order> result = orderServiceUnderTest.update(order);

        result.ifPresent(a -> {
            assertEquals(a.getId(), result.get().getId());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testDelete() {
        orderServiceUnderTest.delete(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));

        verify(mockOrderRepository).deleteById(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
    }

}
