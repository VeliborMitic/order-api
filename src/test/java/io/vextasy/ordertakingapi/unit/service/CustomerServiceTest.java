package io.vextasy.ordertakingapi.unit.service;


import io.vextasy.ordertakingapi.entity.Customer;
import io.vextasy.ordertakingapi.repository.CustomerRepository;
import io.vextasy.ordertakingapi.service.impl.CustomerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class CustomerServiceTest {

    @Mock
    private CustomerRepository mockCustomerRepository;

    @InjectMocks
    private CustomerServiceImpl customerServiceUnderTest;

    private final Customer customer;

    CustomerServiceTest(){
        customer = new Customer();
        customer.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));
        customer.setFirstName("Bob");
        customer.setLastName("Builder");
        customer.setEmail("bob@the-builder.com");
        customer.setPhoneNumber("123456789");
        customer.setBillingAddress("Some Street 111, Somewhere");
    }

    @BeforeEach
    void setUp(){
        initMocks(this);
        customerServiceUnderTest = new CustomerServiceImpl(mockCustomerRepository);

        doReturn(Optional.of(customer)).when(mockCustomerRepository).findById(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));
    }

    @Test
    void testGet() {
        final Optional<Customer> optionalResult = customerServiceUnderTest.get(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));

        optionalResult.ifPresent(a -> {
            assertEquals(a, optionalResult.get());
            assertEquals(a.getEmail(), optionalResult.get().getEmail());
            assertEquals(a.getClass(), optionalResult.get().getClass());
        });
    }

    @Test
    void testGetAll() {
        final List<Customer> customers = new ArrayList<>();
        Customer customer2 = new Customer();
        customer2.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
        customer2.setEmail("alis@x-company.com");
        customer2.setFirstName("Alis");

        customers.add(customer2);
        customers.add(customer);
        doReturn(customers).when(mockCustomerRepository).findAll();

        final List<Customer> result = customerServiceUnderTest.get();

        assertFalse(result.isEmpty());
        assertEquals(result.get(0), customers.get(0));
        assertEquals(2, result.size());
        assertEquals(customers.getClass(), result.getClass());
        assertEquals(customers, result);
    }

    @Test
    void testCreate() {
        doReturn(customer).when(mockCustomerRepository).save(customer);

        final Optional<Customer> result = customerServiceUnderTest.create(customer);

        result.ifPresent(a -> {
            assertEquals(a.getId(), result.get().getId());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testUpdate() {
        Customer updated = new Customer();
        updated.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
        doReturn(updated).when(mockCustomerRepository).save(customer);

        final Optional<Customer> result = customerServiceUnderTest.update(customer);

        result.ifPresent(a -> {
            assertEquals(a.getId(), result.get().getId());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testDelete() {
        customerServiceUnderTest.delete(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));

        verify(mockCustomerRepository).deleteById(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
    }

}

