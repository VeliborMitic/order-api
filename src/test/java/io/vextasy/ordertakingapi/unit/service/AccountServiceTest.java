package io.vextasy.ordertakingapi.unit.service;

import io.vextasy.ordertakingapi.entity.Account;
import io.vextasy.ordertakingapi.entity.Role;
import io.vextasy.ordertakingapi.repository.AccountRepository;
import io.vextasy.ordertakingapi.service.impl.AccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class AccountServiceTest {

    @Mock
    private AccountRepository mockAccountRepository;

    @InjectMocks
    private AccountServiceImpl accountServiceUnderTest;

    private final Account account;

    AccountServiceTest(){
        account = new Account();
        account.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));
        account.setEmail("testAccount@x-company.com");
        account.setVerifiedEmail(true);
        account.setPassword("123456");
        account.setRole(Role.ROLE_ADMIN);
        account.setActive(true);
    }

    @BeforeEach
    void setUp(){
        initMocks(this);
        accountServiceUnderTest = new AccountServiceImpl(mockAccountRepository);

        doReturn(Optional.of(account)).when(mockAccountRepository).findById(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));
    }

    @Test
    void testGet() {
        final Optional<Account> optionalAccount = accountServiceUnderTest.get(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));

        optionalAccount.ifPresent(a -> {
            assertEquals(a, optionalAccount.get());
            assertEquals(a.getEmail(), optionalAccount.get().getEmail());
            assertEquals(a.getClass(), optionalAccount.get().getClass());
        });
    }

    @Test
    void testGetAll() {
        final List<Account> accounts = new ArrayList<>();
        Account account2 = new Account();
        account.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
        account.setEmail("testAccount2@x-company.com");
        account.setVerifiedEmail(true);
        account.setPassword("123456");
        account.setRole(Role.ROLE_ERP_ACCOUNT);
        account.setActive(true);

        accounts.add(account2);
        accounts.add(account);
        doReturn(accounts).when(mockAccountRepository).findAll();

        final List<Account> result = accountServiceUnderTest.get();

        assertFalse(result.isEmpty());
        assertEquals(result.get(0), accounts.get(0));
        assertEquals(2, result.size());
        assertEquals(accounts.getClass(), result.getClass());
        assertEquals(accounts, result);
    }

    @Test
    void testCreate() {
        doReturn(account).when(mockAccountRepository).save(account);

        final Optional<Account> result = accountServiceUnderTest.create(account);

        result.ifPresent(a -> {
            assertEquals(a.getId(), result.get().getId());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testUpdate() {
        Account updated = new Account();
        updated.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
        doReturn(updated).when(mockAccountRepository).save(account);

        final Optional<Account> result = accountServiceUnderTest.update(account);

        result.ifPresent(a -> {
            assertEquals(a.getId(), result.get().getId());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testDelete() {
        accountServiceUnderTest.delete(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));

        verify(mockAccountRepository).deleteById(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
    }

}
