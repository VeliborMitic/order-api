package io.vextasy.ordertakingapi.unit.service;

import io.vextasy.ordertakingapi.entity.Product;
import io.vextasy.ordertakingapi.entity.ProductType;
import io.vextasy.ordertakingapi.repository.ProductRepository;
import io.vextasy.ordertakingapi.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
class ProductServiceTest {

    @Mock
    private ProductRepository mockProductRepository;

    @InjectMocks
    private ProductServiceImpl productServiceUnderTest;

    private final Product product;

    ProductServiceTest(){
        product = new Product();
        product.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));
        product.setName("50 Mbs");
        product.setProductType(ProductType.INTERNET);
        product.setDescription("Internet - 50 Mbs");
        product.setPrice(BigDecimal.valueOf(10));
    }

    @BeforeEach
    void setUp(){
        initMocks(this);
        productServiceUnderTest = new ProductServiceImpl(mockProductRepository);

        doReturn(Optional.of(product)).when(mockProductRepository).findById(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));
    }

    @Test
    void testGet() {
        final Optional<Product> optionalResult = productServiceUnderTest.get(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fce"));

        optionalResult.ifPresent(a -> {
            assertEquals(a, optionalResult.get());
            assertEquals(a.getPrice(), optionalResult.get().getPrice());
            assertEquals(a.getProductType(), optionalResult.get().getProductType());
            assertEquals(a.getClass(), optionalResult.get().getClass());
        });
    }

    @Test
    void testGetAll() {
        final List<Product> products = new ArrayList<>();
        Product product2 = new Product();
        product2.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
        product2.setProductType(ProductType.TELEPHONY);

        products.add(product);
        products.add(product2);
        doReturn(products).when(mockProductRepository).findAll();

        final List<Product> results = productServiceUnderTest.get();

        assertFalse(results.isEmpty());
        assertEquals(results.get(0), products.get(0));
        assertEquals(2, results.size());
        assertEquals(products.getClass(), results.getClass());
        assertEquals(products, results);
        assertNotEquals(results.get(0).getProductType(), results.get(1).getProductType());
    }

    @Test
    void testCreate() {
        doReturn(product).when(mockProductRepository).save(product);

        final Optional<Product> result = productServiceUnderTest.create(product);

        result.ifPresent(a -> {
            assertEquals(a.getId(), result.get().getId());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testUpdate() {
        Product updated = new Product();
        updated.setId(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
        doReturn(updated).when(mockProductRepository).save(product);

        final Optional<Product> result = productServiceUnderTest.update(product);

        result.ifPresent(a -> {
            assertEquals(a.getId(), result.get().getId());
            assertEquals(a.getClass(), result.get().getClass());
            assertEquals(a, result.get());
        });
    }

    @Test
    void testDelete() {
        productServiceUnderTest.delete(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));

        verify(mockProductRepository).deleteById(UUID.fromString("7644af5e-0b28-4914-b8ec-ff8359317fcf"));
    }

}
